package com.premasters.seinsights;

import android.test.AndroidTestCase;
import android.util.Log;

import com.premasters.seinsights.db.Category;
import com.premasters.seinsights.db.Role;
import com.premasters.seinsights.db.Topic;
import com.premasters.seinsights.db.User;

import org.junit.Before;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

/**
 * Created by Timur on 07.04.2016.
 */
public class NetworkControllerTest extends AndroidTestCase {

    NetworkController networkController;
    int topicId, userId, roleId, categoryId;
    String testText, testTitle, testDate, wrongId, wrongDate, testPassword, testCode, testFormatedDate;

    @Before
    public void setUp() throws Exception {
        networkController = new NetworkController(this.getContext());
        testText = "TextTest";
        testTitle = "TitleTest";
        testDate = "2015-07-19 01:23:45.6";
        wrongId = "wrong id";
        wrongDate = "wrong date";
        testPassword = "testpassword";
        testCode = "testcode";
        testFormatedDate = "2015-07-19%2001%3A23%3A45.6";
    }

    @Test
    public void testTopic() {

        int localTopicId;

        try {
            String responseCreate = networkController.createUpdateTopic("1", testTitle, testText,
                    "0", testDate, testDate);
            String[] temp = responseCreate.substring(17).split(" ");
            localTopicId = Integer.parseInt(temp[0]);
            assertEquals('0', responseCreate.charAt(0));

            String responseUpdate = networkController.createUpdateTopic("" + localTopicId, "NewTitleTest", "NewTextTest",
                    "0", testDate, "2015-07-19 01:23:45.7");
            assertEquals('0', responseUpdate.charAt(0));

            Topic topic = networkController.getTopicById("" + localTopicId);
            assertEquals(localTopicId, topic.getId());

            networkController.deleteTopic("" + localTopicId);
            assertEquals("true", networkController.getTopicById("" + localTopicId).isDeleted());

        } catch (UnsupportedEncodingException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
    }

    @Test
    public void testIncorrectCreateTopic() {

        try {
            String response = networkController.createUpdateTopic("1", testTitle, testText,
                    "wrong likes", testDate, testDate);
            assertEquals('{', response.charAt(0));
            response = networkController.createUpdateTopic("1", testTitle, testText,
                    "0", wrongDate, testDate);
            assertEquals('{', response.charAt(0));
            response = networkController.createUpdateTopic("1", testTitle, testText,
                    "0", testDate, wrongDate);
            assertEquals('{', response.charAt(0));
        } catch (UnsupportedEncodingException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
    }


    @Test
    public void testGetAllTopics() {
        List<Topic> topics = networkController.getAllTopics();
        assertNotNull(topics);
    }

    @Test
    public void testGetRoles() {
        List<Role> roles = networkController.getRoles();
        assertNotNull(roles);
    }

    @Test
    public void testUserRoles() {
        int localUserId, localRoleId;
        //create user
        String response = networkController.createUpdateUser(null, "testemail@testemail.com",
                testDate, "name", testPassword, "2", testCode);
        String[] temp = response.substring(16).split(" ");
        localUserId = Integer.parseInt(temp[0]);
        //get user
        User user = networkController.getUserById("" + localUserId);
        assertNotNull(user);
        //create role
        response = networkController.createRole("testrole");
        temp = response.substring(16).split(" ");
        localRoleId = Integer.parseInt(temp[0]);
        assertEquals('0', response.charAt(0));
        //create userrole
        response = networkController.createDeleteUserRole("" + localRoleId, "" + localUserId);
        assertEquals('0', response.charAt(0));
        //update user
        response = networkController.createUpdateUser("" + localUserId, "testemail3@testemail.ru",
                testDate, "newtestname", testPassword, "2", testCode);
        assertEquals('0', response.charAt(0));
        //update role
        response = networkController.updateRole("" + localRoleId, "newtestrole");
        assertEquals('0', response.charAt(0));
        //delete userrole
        response = networkController.createDeleteUserRole("" + localRoleId, "" + localUserId);
        assertEquals('0', response.charAt(0));
        //delete user
        response = networkController.deleteUser("" + localUserId);
        assertEquals('0', response.charAt(0));
        //delete role
        response = networkController.deleteRole("" + localRoleId);
        assertEquals('0', response.charAt(0));
    }

    @Test
    public void testIncorrectUserRoles() {
        //incorrect create user
        String response = networkController.createUpdateUser(null, "wrong email",
                wrongDate, "testname3", testPassword, "2", testCode);
        assertEquals('{', response.charAt(0));
        //incorrect create userrole
        response = networkController.createDeleteUserRole(wrongId, wrongId);
        assertEquals('{', response.charAt(0));
        //incorrect get user
        User user = networkController.getUserById(wrongId);
        assertNull(user);
        //incorrect update user
        response = networkController.createUpdateUser(wrongId, "wrong email",
                testDate, "newtestname", testPassword, "2", testCode);
        assertEquals('{', response.charAt(0));
        //incorrect update role
        response = networkController.updateRole(wrongId, "newtestrole");
        assertEquals('{', response.charAt(0));
        //incorrect delete user
        response = networkController.deleteUser(wrongId);
        assertEquals('{', response.charAt(0));
        //incorrect delete role
        response = networkController.deleteRole(wrongId);
        assertEquals('{', response.charAt(0));
    }

    @Test
    public void testGetNewTopics() {
        List<Topic> topics = networkController.getNewTopics(testFormatedDate);
        assertNotNull(topics);
    }

    @Test
    public void testGetNewCategories() {
        List<Category> categories = networkController.getNewCategories(testFormatedDate);
        assertNotNull(categories);
    }

    @Test
    public void testSyncTopics() {
        try {
            assertTrue(networkController.syncTopics(this.getContext(), networkController.getAllTopics()));
        } catch (UnsupportedEncodingException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
    }

    @Test
    public void testLikeDislikeTopic() {

        String response = null;
        try {
            response = networkController.createUpdateTopic("1", testTitle, testText,
                    "0", testDate, testDate);
        } catch (UnsupportedEncodingException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        String[] temp = response.substring(17).split(" ");
        topicId = Integer.parseInt(temp[0]);

        response = networkController.createUpdateUser(null, "testemail@testemail.com",
                testDate, "name", testPassword, "2", testCode);
        temp = response.substring(16).split(" ");
        userId = Integer.parseInt(temp[0]);

        int likes = networkController.getTopicById("" + topicId).getLikes();
        networkController.likeTopic("" + topicId, "" + userId, testFormatedDate);
        assertEquals(likes + 1, networkController.getTopicById("" + topicId).getLikes());
        networkController.dislikeTopic("" + topicId, "" + userId);
        assertEquals(likes, networkController.getTopicById("" + topicId).getLikes());

        try {
            networkController.deleteTopic("" + topicId);
            networkController.deleteUser("" + userId);
        } catch (UnsupportedEncodingException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
    }

    @Test
    public void testCategory() {
        int localCategoryId;
        //create category
        String response = networkController.createCategory("Test" + (new Date()).toString());
        String[] temp = response.substring(20).split(" ");
        localCategoryId = Integer.parseInt(temp[0]);
        assertEquals('0', response.charAt(0));
        //update category
        response = networkController.updateCategory("" + localCategoryId, "NewName");
        assertEquals('0', response.charAt(0));
        //delete category
        response = networkController.deleteCategory("" + localCategoryId);
        assertEquals('0', response.charAt(0));
    }

    @Test
    public void testIncorrectCategory() {
        //incorrect update category
        String response = networkController.updateCategory(wrongId, "NewName");
        assertEquals('{', response.charAt(0));
        //incorrect delete category
        response = networkController.deleteCategory(wrongId);
        assertEquals('{', response.charAt(0));
    }

    @Test
    public void testTopicCategory() {
        String response = null;
        try {
            response = networkController.createUpdateTopic("1", testTitle, testText,
                    "0", testDate, testDate);
        } catch (UnsupportedEncodingException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        String[] temp = response.substring(17).split(" ");
        topicId = Integer.parseInt(temp[0]);

        response = networkController.createCategory("Test");
        temp = response.substring(20).split(" ");
        categoryId = Integer.parseInt(temp[0]);

        //create topiccategory
        response = networkController.createDeleteTopicCategory("" + topicId, "" + categoryId);
        assertEquals('0', response.charAt(0));
        //delete topiccategory
        response = networkController.createDeleteTopicCategory("" + topicId, "" + categoryId);
        assertEquals('0', response.charAt(0));

        try {
            networkController.deleteTopic("" + topicId);
            networkController.deleteCategory("" + categoryId);
        } catch (UnsupportedEncodingException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }

    }

    @Test
    public void testIncorrectTopicCategory() {
        //incorrect create topiccategory
        String response = networkController.createDeleteTopicCategory(wrongId, wrongId);
        assertEquals('{', response.charAt(0));
        //incorrect delete topiccategory
        response = networkController.createDeleteTopicCategory(wrongId, wrongId);
        assertEquals('{', response.charAt(0));
    }
}
