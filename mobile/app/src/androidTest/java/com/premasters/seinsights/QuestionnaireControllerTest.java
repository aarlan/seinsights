package com.premasters.seinsights;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by Timur on 11.02.2016.
 */
public class QuestionnaireControllerTest extends TestCase {

    QuestionnaireController questionnaireController;


    @Before
    public void setUp() throws Exception {
        questionnaireController = new QuestionnaireController();
        questionnaireController.fillQuestionsArray();
    }

    @Test
    public void testLoadQuestions() throws Exception {

        String string = questionnaireController.storeQuestions();
        QuestionnaireController testQuestionaireConrtoller = new QuestionnaireController();
        testQuestionaireConrtoller.loadQuestions(string);

        assertEquals(questionnaireController.getQuestions(), testQuestionaireConrtoller.getQuestions());
    }
}