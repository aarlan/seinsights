package com.premasters.seinsights;

import android.test.AndroidTestCase;

import com.premasters.seinsights.db.Topic;
import com.premasters.seinsights.db.TopicDAO;

import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Alexey Merzlikin on 30.03.2016.
 */
public class DatabaseSyncTest extends AndroidTestCase {

    DatabaseSync databaseSync;
    List<Topic> topics;

    @Before
    public void setUp() throws Exception {
        databaseSync = new DatabaseSync();
        topics = new ArrayList<>();
    }

    @Test
    public void testUpload() throws ParseException {
        String title = "topic for unit test3";
        Date date = new Date();
        String dateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(date);
        TopicDAO topicDAO = new TopicDAO(this.getContext());

        List<Topic> topicsFromDB = (List<Topic>) topicDAO.findAll();
        int size = topicsFromDB.size();

        Topic topic = new Topic(null,
                title,
                title,
                0,
                dateString,
                dateString,
                false, true);
        topics.add(topic);
        databaseSync.updateDatabase(topics);

        topicsFromDB = (List<Topic>) topicDAO.findAll();
        int newSize = topicsFromDB.size();
        assertEquals(size, newSize - 1);
        assertEquals(title, topicsFromDB.get(size).getTitle());
    }
}
