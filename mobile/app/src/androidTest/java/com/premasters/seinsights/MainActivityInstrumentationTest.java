package com.premasters.seinsights;

/**
 * Created by Alexey Merzlikin on 28.01.2016.
 */

import android.os.SystemClock;
import android.support.test.rule.ActivityTestRule;
import android.test.AndroidTestCase;

import com.premasters.seinsights.db.Topic;
import com.premasters.seinsights.db.TopicDAO;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

// Tests for MainActivity
public class MainActivityInstrumentationTest extends AndroidTestCase {

    // Preferred JUnit 4 mechanism of specifying the activity to be launched before each test
    @Rule
    public ActivityTestRule<MainActivity> activityTestRule =
            new ActivityTestRule<>(MainActivity.class);


    @Test
    public void validateTest() {
        SystemClock.sleep(5000);

        onView(withId(R.id.questbut))
                .perform(click());

        for (int i = 0; i < 11; i++) {
            onView(withId(R.id.radioGroup1))
                    .check(matches(isEnabled()));

            if (i == 1) {
                onView(withId(R.id.prev_button))
                        .perform(click());
                onView(withId(R.id.answerA))
                        .perform(click());
            }

            if (i == 9) {
                for (int k = 0; k < 6; k++) {
                    onView(withId(R.id.prev_button))
                            .perform(click());
                    SystemClock.sleep(1000);
                }
                for (int j = 0; j < 6; j++) {
                    onView(withId(R.id.next_button))
                            .perform(click());
                    SystemClock.sleep(1000);
                }
            }

            if (i % 2 == 0) {
                onView(withId(R.id.answerA))
                        .perform(click());
            } else if (i == 4) {
                onView(withId(R.id.answerD))
                        .perform(click());
            } else if (i == 2) {
                onView(withId(R.id.answerC))
                        .perform(click());
            } else {
                onView(withId(R.id.answerB))
                        .perform(click());
            }
            SystemClock.sleep(1000);
        }

        //Result
        onView(withId(R.id.answA))
                .check(matches(isDisplayed()));
        onView(withId(R.id.answC))
                .check(matches(withText("23")));
        //After result
        onView(withId(R.id.button2))
                .perform(click());
    }


    @Ignore
    public void validateTest2() {
        SystemClock.sleep(5000);

        onView(withId(R.id.questbut))
                .perform(click());

        for (int i = 0; i < 11; i++) {
            if (i % 2 == 0) {
                onView(withId(R.id.answerA))
                        .perform(click());
            } else {
                onView(withId(R.id.answerB))
                        .perform(click());
            }
            SystemClock.sleep(1000);
        }

        //Result
        onView(withId(R.id.answA))
                .check(matches(isDisplayed()));
        onView(withId(R.id.answC))
                .check(matches(withText("23")));
        //After result
        onView(withId(R.id.button2))
                .perform(click());
    }

    @Ignore
    public void validateTopicOfTheDay() {
        SystemClock.sleep(5000);

        String title = "TestTitle";

        onView(withId(R.id.topicbut))
                .perform(click());
        onView(withId(R.id.button))
                .perform(click());

        onView(withId(R.id.search))
                .perform(click());

        //Add new topic and check its existence in db
//        onView(withId(R.id.add))
//                .perform(click());
//        onView(withId(R.id.edit_category))
//                .perform(typeText("TestCategory"));
//        onView(withId(R.id.edit_title))
//                .perform(typeText(title));
//        onView(withId(R.id.edit_text))
//                .perform(typeText("TestTextTestTextTestTextTestTextTestTextTestText"));
//        onView(withId(R.id.button3))
//                .perform(click());
//        TopicDAO topicDAO = new TopicDAO(this.getContext());
//        List<Topic> topics = (List<Topic>) topicDAO.findAll();
//        assertEquals(title, topics.get(topics.size() - 1).getTitle());
    }
}