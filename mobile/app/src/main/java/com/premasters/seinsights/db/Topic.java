package com.premasters.seinsights.db;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by Alexey Merzlikin on 03.03.2016.
 */
@DatabaseTable(tableName = "Topics")
public class Topic {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String text;

    @DatabaseField
    private String title;

    @DatabaseField
    private int likes;

    @DatabaseField
    private Date created;

    @DatabaseField
    private Date modified;

    @DatabaseField
    private boolean deleted;

    @DatabaseField
    private boolean ready;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true)
    private Category category;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true)
    private User user;

    private final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.S";

    Topic() {
        // all persisted classes must define a no-arg constructor with at least package visibility
    }

    public Topic(Category category, String text, String title, int likes, String createdStr, String modifiedStr, boolean deleted, boolean ready) throws ParseException {
        this.category = category;
        this.text = text;
        this.title = title;
        this.likes = likes;
        this.created = new SimpleDateFormat(DATE_FORMAT).parse(createdStr);
        this.modified = new SimpleDateFormat(DATE_FORMAT).parse(modifiedStr);
        this.deleted = deleted;
        this.ready = ready;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public String getCreated() {
        return new SimpleDateFormat(DATE_FORMAT).format(created);
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getModified() {
        return new SimpleDateFormat(DATE_FORMAT).format(modified);
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

}
