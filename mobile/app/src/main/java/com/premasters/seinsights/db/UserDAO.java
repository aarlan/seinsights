package com.premasters.seinsights.db;

/**
 * Created by Alexey Merzlikin on 02.03.2016.
 */

import android.content.Context;
import android.util.Log;

import java.sql.SQLException;
import java.util.List;

public class UserDAO implements Crud {

    private DatabaseHelper helper;
    private final String DAO_ERROR = "DAO_ERROR";

    public UserDAO(Context context) {
        DatabaseManager.init(context);
        helper = DatabaseManager.getInstance().getHelper();
    }

    @Override
    public int create(Object item) {

        int index = -1;

        User object = (User) item;
        try {
            index = helper.getUserDao().create(object);
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "SQL Exception in User DAO");
        }

        return index;
    }

    @Override
    public int update(Object item) {

        int index = -1;

        User object = (User) item;

        try {
            helper.getUserDao().update(object);
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "SQL Exception in User DAO");
        }

        return index;
    }

    @Override
    public int delete(Object item) {

        int index = -1;

        User object = (User) item;

        try {
            helper.getUserDao().delete(object);
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "SQL Exception in User DAO");
        }

        return index;

    }

    @Override
    public Object findById(int id) {

        User wishList = null;
        try {
            wishList = helper.getUserDao().queryForId(id);
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "SQL Exception in User DAO");
        }
        return wishList;

    }

    @Override
    public List<?> findAll() {

        List<User> items = null;

        try {
            items = helper.getUserDao().queryForAll();
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "SQL Exception in User DAO");
        }

        return items;
    }

}