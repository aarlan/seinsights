package com.premasters.seinsights.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Database helper class used to manage the creation and upgrading of your database. This class also usually provides
 * the DAOs used by the other classes.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    // name of the database file for your application -- change to something appropriate for your app
    private static final String DATABASE_NAME = "mobile9.db";
    // any time you make changes to your database objects, you may have to increase the database version
    private static final int DATABASE_VERSION = 1;
    private static final String DB_HELPER_ERROR = "DB_HELPER_ERROR";
    // the DAO object we use to access the SimpleData table
    private RuntimeExceptionDao<User, Integer> simpleRuntimeDao = null;

    private Dao<User, Integer> userDAO = null;
    private Dao<Role, Integer> roleDAO = null;
    private Dao<Topic, Integer> topicDAO = null;
    private Dao<Category, Integer> categoryDAO = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public Dao<User, Integer> getuserDAO() {
        if (null == userDAO) {
            try {
                userDAO = getDao(User.class);
            } catch (java.sql.SQLException e) {
                Log.d(DB_HELPER_ERROR, "SQL exception in database helper");
            }
        }
        return userDAO;
    }

    /**
     * This is called when the database is first created. Usually you should call createTable statements here to create
     * the tables that will store your data.
     */
    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            Log.d(DB_HELPER_ERROR, "onCreate");
            TableUtils.createTable(connectionSource, Category.class);
            TableUtils.createTable(connectionSource, Topic.class);
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, Role.class);
        } catch (SQLException e) {
            Log.d(DB_HELPER_ERROR, "Can't create database", e);
        }
    }

    /**
     * This is called when your application is upgraded and it has a higher version number. This allows you to adjust
     * the various data to match the new version number.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            Log.i(DatabaseHelper.class.getName(), "onUpgrade");
            TableUtils.dropTable(connectionSource, Category.class, true);
            TableUtils.dropTable(connectionSource, Topic.class, true);
            TableUtils.dropTable(connectionSource, User.class, true);
            TableUtils.dropTable(connectionSource, Role.class, true);
            // after we drop the old databases, we create the new ones
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
        }
    }

    /**
     * Returns the Database Access Object (DAO) for our SimpleData class. It will create it or just give the cached
     * value.
     */
    public Dao<User, Integer> getUserDao() throws SQLException {
        if (userDAO == null) {
            userDAO = getDao(User.class);
        }
        return userDAO;
    }

    public Dao<Role, Integer> getRoleDao() throws SQLException {
        if (roleDAO == null) {
            roleDAO = getDao(Role.class);
        }
        return roleDAO;
    }

    public Dao<Topic, Integer> getTopicDao() throws SQLException {
        if (topicDAO == null) {
            topicDAO = getDao(Topic.class);
        }
        return topicDAO;
    }

    public Dao<Category, Integer> getCategoryDAO() throws SQLException {
        if (categoryDAO == null) {
            categoryDAO = getDao(Category.class);
        }
        return categoryDAO;
    }

    /**
     * Returns the RuntimeExceptionDao (Database Access Object) version of a Dao for our SimpleData class. It will
     * create it or just give the cached value. RuntimeExceptionDao only through RuntimeExceptions.
     */
    public RuntimeExceptionDao<User, Integer> getSimpleDataDao() {
        if (simpleRuntimeDao == null) {
            simpleRuntimeDao = getRuntimeExceptionDao(User.class);
        }
        return simpleRuntimeDao;
    }

    /**
     * Close the database connections and clear any cached DAOs.
     */
    @Override
    public void close() {
        super.close();
        simpleRuntimeDao = null;
    }
}
