package com.premasters.seinsights;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.SSLCertificateSocketFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.premasters.seinsights.db.Category;
import com.premasters.seinsights.db.Role;
import com.premasters.seinsights.db.Topic;
import com.premasters.seinsights.db.User;

import org.apache.commons.io.IOUtils;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Timur on 10.03.2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NetworkController {

    static Context context;

    private static final String CONNECTION_PROTOCOL = "https";
    private static final String IP = Constants.IP;
    private static final String PORT = "9000";
    private static final char PARAMETER_DELIMITER = '&';
    private static final char PARAMETER_EQUALS_CHAR = '=';
    private static final String ENCODING = "UTF-8";

    static SharedPreferences sPref;
    final static String email = "email";
    final static String password = "password";
    final static String role = "role";

    public static String loadUserRole() throws UnsupportedEncodingException {
        sPref = context.getSharedPreferences("userData", Activity.MODE_PRIVATE);
        String hashedRole = sPref.getString(role, "");
        byte[] bytesRole = Base64.decode(hashedRole, Base64.DEFAULT);
        return new String(bytesRole, "UTF-8");
    }

    public static String loadUserEmail(boolean isForAuthentication) throws UnsupportedEncodingException {
        if (isForAuthentication) { // Then Guest
            return "seinsightsguest@alnedorezov.com";
        } else {
            sPref = context.getSharedPreferences("userData", Activity.MODE_PRIVATE);
            String hashedRole = sPref.getString(role, "");
            byte[] bytesRole = Base64.decode(hashedRole, Base64.DEFAULT);
            String userRole = new String(bytesRole, "UTF-8");
            if (userRole.equals("2") || userRole.equals("3")) { // If Reader or Administrator
                String hashedEmail = sPref.getString(email, "");
                byte[] bytesEmail = Base64.decode(hashedEmail, Base64.DEFAULT);
                return new String(bytesEmail, "UTF-8").toLowerCase();
            } else if (userRole.equals("1")) { // If Guest
                return "seinsightsguest@alnedorezov.com";
            } else
                throw new IllegalStateException();
        }
    }

    public static String loadUserPwd(boolean isForAuthentication) throws UnsupportedEncodingException {
        if (isForAuthentication) { // Then Guest
            return "Guest123!";
        } else {
            sPref = context.getSharedPreferences("userData", Activity.MODE_PRIVATE);
            String hashedRole = sPref.getString(role, "");
            byte[] bytesRole = Base64.decode(hashedRole, Base64.DEFAULT);
            String userRole = new String(bytesRole, "UTF-8");
            if (userRole.equals("2") || userRole.equals("3")) { // If Reader or Administrator
                String hashedPassword = sPref.getString(password, "");
                byte[] bytesPassword = Base64.decode(hashedPassword, Base64.DEFAULT);
                return new String(bytesPassword, "UTF-8");
            } else if (userRole.equals("1")) { // If Guest
                return "Guest123!";
            } else
                throw new IllegalStateException();
        }
    }

    public NetworkController(Context context) {
        this.context = context;
    }

    private static String createQueryStringForParameters(Map<String, String> parameters) {
        StringBuilder parametersAsQueryString = new StringBuilder();
        if (parameters != null) {
            boolean firstParameter = true;

            for (String parameterName : parameters.keySet()) {
                if (!firstParameter) {
                    parametersAsQueryString.append(PARAMETER_DELIMITER);
                }

                try {
                    parametersAsQueryString.append(parameterName)
                            .append(PARAMETER_EQUALS_CHAR)
                            .append(URLEncoder.encode(parameters.get(parameterName), ENCODING));
                } catch (UnsupportedEncodingException e) {
                    Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
                }

                firstParameter = false;
            }
        }
        return parametersAsQueryString.toString();
    }

    private static String establishPostConnection(String targetURL, String urlParams, final boolean isForAuthentication) {
        URL url;
        HttpsURLConnection connection = null;
        try {
            //Create connection
            url = new URL(targetURL);
            connection = (HttpsURLConnection) url.openConnection();
            Authenticator.setDefault(new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    try {
                        if (isForAuthentication)
                            return new PasswordAuthentication(loadUserEmail(true), loadUserPwd(true).toCharArray());
                        else
                            return new PasswordAuthentication(loadUserEmail(false), loadUserPwd(false).toCharArray());
                    } catch (UnsupportedEncodingException e) {
                        Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
                    }
                    throw new IllegalStateException();
                }
            });

            connection.setSSLSocketFactory(SSLCertificateSocketFactory.getInsecure(0, null));
            connection.setHostnameVerifier(new AllowAllHostnameVerifier());

            if (urlParams != null) {
                connection.setRequestMethod("POST");
                connection.setDoInput(true);
                connection.setDoOutput(true);

                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

                connection.connect();

                //send the POST out
                DataOutputStream dStream = new DataOutputStream(connection.getOutputStream());
                dStream.writeBytes(urlParams);
                dStream.flush();

                int status = connection.getResponseCode();

                BufferedReader br;
                if (status >= HttpURLConnection.HTTP_BAD_REQUEST)
                    br = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                else
                    br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                String line;
                StringBuilder responseOutput = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    responseOutput.append(line);
                }
                String response = responseOutput.toString();
                br.close();
                dStream.close();
                return response;
            }

        } catch (IOException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return null;
    }

    private String establishGetConnection(String urlString, final String defaultUsername, final String defaultPassword) {
        try {
            URL url = new URL(urlString);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            Authenticator.setDefault(new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(defaultUsername, defaultPassword.toCharArray());
                }
            });
            if (connection instanceof HttpsURLConnection) {
                HttpsURLConnection httpsConn = (HttpsURLConnection) connection;
                httpsConn.setSSLSocketFactory(SSLCertificateSocketFactory.getInsecure(0, null));
                httpsConn.setHostnameVerifier(new AllowAllHostnameVerifier());
            }
            InputStream is = connection.getInputStream();

            return IOUtils.toString(is, ENCODING);

        } catch (IOException e) {
            Log.e(Constants.LOG, e.getMessage());
        }
        return null;
    }

    public Topic getTopicById(String id) {
        try {
            return new GetTopicByIdTask().execute(id).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public List<Topic> getAllTopics() {
        try {
            return new GetAllTopicsTask().execute().get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return Collections.emptyList();
    }

    public List<Role> getRoles() {
        try {
            return new GetRolesTask().execute().get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return Collections.emptyList();
    }

    public User getUserById(String id) {
        try {
            return new GetUserByIdTask().execute(id).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public List<Topic> getNewTopics(String date) {
        try {
            List<Id> topicIdList = new GetTopicSyncTask().execute(formatDate(date)).get();
            List<Topic> topicList = new ArrayList<>();
            if (topicIdList != null) {
                for (Id topicId : topicIdList) {
                    topicList.add(new GetTopicByIdTask().execute("" + topicId.getId()).get());
                }
            }
            return topicList;
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return Collections.emptyList();
    }

    public List<Category> getNewCategories(String date) {
        try {
            List<Id> categoryIdList = new GetCategorySyncTask().execute(date).get();
            List<Category> categoryList = new ArrayList<>();
            for (Id categoryId : categoryIdList) {
                categoryList.add(new GetCategoryByIdTask().execute("" + categoryId.getId()).get());
            }
            return categoryList;
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return Collections.emptyList();
    }

    public String deleteTopic(String id) throws UnsupportedEncodingException {
        Map<String, String> urlParametersMap = new HashMap<>();
        urlParametersMap.put("id", id);
        urlParametersMap.put("deleted", "true");
        String urlParameters = createQueryStringForParameters(urlParametersMap);

        try {
            return new CRUDTopicTask().execute(urlParameters).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public boolean syncTopics(Context context, List<Topic> topics) throws UnsupportedEncodingException {
        if (checkConnection(context)) {
            for (Topic topic : topics) {
                if (topic != null) {
                    createUpdateTopic("" + topic.getId(), topic.getTitle(), topic.getText(),
                            "" + topic.getLikes(), topic.getCreated(), topic.getModified());
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public String createUpdateTopic(String id, String title, String text, String likes, String created,
                                    String modified) throws UnsupportedEncodingException {
        Map<String, String> urlParametersMap = new HashMap<>();
        urlParametersMap.put("id", id);
        urlParametersMap.put("title", title);
        urlParametersMap.put("text", text);
        urlParametersMap.put("likes", likes);
        urlParametersMap.put("created", created);
        urlParametersMap.put("modified", modified);
        urlParametersMap.put("deleted", "false");
        urlParametersMap.put("ready", "true");
        String urlParameters = createQueryStringForParameters(urlParametersMap);

        try {
            return new CRUDTopicTask().execute(urlParameters).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public String likeTopic(String topicId, String userId, String created) {
        Map<String, String> urlParametersMap = new HashMap<>();
        urlParametersMap.put("topicid", topicId);
        urlParametersMap.put("userid", userId);
        urlParametersMap.put("created", created);
        String urlParameters = createQueryStringForParameters(urlParametersMap);
        try {
            return new LikeDislikeTopicTask().execute(urlParameters).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public String dislikeTopic(String topicId, String userId) {
        Map<String, String> urlParametersMap = new HashMap<>();
        urlParametersMap.put("topicid", topicId);
        urlParametersMap.put("userid", userId);
        String urlParameters = createQueryStringForParameters(urlParametersMap);
        try {
            return new LikeDislikeTopicTask().execute(urlParameters).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public String deleteCategory(String categoryId) {
        Map<String, String> urlParametersMap = new HashMap<>();
        urlParametersMap.put("id", categoryId);
        String urlParameters = createQueryStringForParameters(urlParametersMap);
        try {
            return new CRUDCategoryTask().execute(urlParameters).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public String createCategory(String categoryName) {
        Map<String, String> urlParametersMap = new HashMap<>();
        urlParametersMap.put("name", categoryName);
        String urlParameters = createQueryStringForParameters(urlParametersMap);
        try {
            return new CRUDCategoryTask().execute(urlParameters).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public String updateCategory(String categoryId, String categoryName) {
        Map<String, String> urlParametersMap = new HashMap<>();
        urlParametersMap.put("id", categoryId);
        urlParametersMap.put("name", categoryName);
        String urlParameters = createQueryStringForParameters(urlParametersMap);
        try {
            return new CRUDCategoryTask().execute(urlParameters).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public String createDeleteTopicCategory(String topicId, String categoryId) {
        Map<String, String> urlParametersMap = new HashMap<>();
        urlParametersMap.put("topicid", topicId);
        urlParametersMap.put("categoryId", categoryId);
        String urlParameters = createQueryStringForParameters(urlParametersMap);
        try {
            return new CreateDeleteTopicCategoryTask().execute(urlParameters).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public String createRole(String name) {
        Map<String, String> urlParametersMap = new HashMap<>();
        urlParametersMap.put("name", name);
        String urlParameters = createQueryStringForParameters(urlParametersMap);
        try {
            return new CRUDRoleTask().execute(urlParameters).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public String updateRole(String id, String name) {
        Map<String, String> urlParametersMap = new HashMap<>();
        urlParametersMap.put("id", id);
        urlParametersMap.put("name", name);
        String urlParameters = createQueryStringForParameters(urlParametersMap);
        try {
            return new CRUDRoleTask().execute(urlParameters).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public String deleteRole(String id) {
        Map<String, String> urlParametersMap = new HashMap<>();
        urlParametersMap.put("id", id);
        String urlParameters = createQueryStringForParameters(urlParametersMap);
        try {
            return new CRUDRoleTask().execute(urlParameters).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public String createDeleteUserRole(String roleId, String userId) {
        Map<String, String> urlParametersMap = new HashMap<>();
        urlParametersMap.put("roleid", roleId);
        urlParametersMap.put("userid", userId);
        String urlParameters = createQueryStringForParameters(urlParametersMap);
        try {
            return new CreateDeleteUserRoleTask().execute(urlParameters).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    //pass id=null when you want to create User
    public String createUpdateUser(String id, String email, String created, String name, String password,
                                   String activated, String activationCode) {
        Map<String, String> urlParametersMap = new HashMap<>();
        if (id != null) {
            urlParametersMap.put("id", id);
        }
        urlParametersMap.put("email", email.toLowerCase());
        urlParametersMap.put("created", created);
        urlParametersMap.put("name", name);
        urlParametersMap.put("password", password);
        urlParametersMap.put("activated", activated);
        urlParametersMap.put("activationCode", activationCode);
        urlParametersMap.put("deleted", "false");
        String urlParameters = createQueryStringForParameters(urlParametersMap);
        try {
            return new CRUDUserTask().execute(urlParameters).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public String deleteUser(String id) {
        Map<String, String> urlParametersMap = new HashMap<>();
        urlParametersMap.put("id", id);
        urlParametersMap.put("deleted", "true");
        String urlParameters = createQueryStringForParameters(urlParametersMap);
        try {
            return new CRUDUserTask().execute(urlParameters).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public boolean checkConnection(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public String authenticate(String email, String password) {
        Map<String, String> urlParametersMap = new HashMap<>();
        urlParametersMap.put("email", email.toLowerCase());
        urlParametersMap.put("password", password);
        String urlParameters = createQueryStringForParameters(urlParametersMap);
        try {
            return new AuthTask().execute(urlParameters).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public String sendActivationCode(String email, String password, String activationCode) {
        Map<String, String> urlParametersMap = new HashMap<>();
        urlParametersMap.put("email", email.toLowerCase());
        urlParametersMap.put("password", password);
        urlParametersMap.put("activationcode", activationCode);
        String urlParameters = createQueryStringForParameters(urlParametersMap);
        try {
            return new ActivationCodeTask().execute(urlParameters).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public String register(String email, String password, String name) {
        Map<String, String> urlParametersMap = new HashMap<>();
        urlParametersMap.put("email", email.toLowerCase());
        urlParametersMap.put("password", password);
        urlParametersMap.put("name", name);
        String urlParameters = createQueryStringForParameters(urlParametersMap);
        try {
            return new RegisterTask().execute(urlParameters).get();
        } catch (InterruptedException | ExecutionException e) {
            Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
        }
        return null;
    }

    public String formatDate(String date) {
        if (date != null) {
            String formatedDate = date;
            formatedDate = formatedDate.replaceAll(" ", "%20");
            formatedDate = formatedDate.replaceAll(":", "%3A");
            return formatedDate;
        } else {
            return null;
        }
    }

    private class GetTopicByIdTask extends AsyncTask<String, Void, Topic> {
        @Override
        protected Topic doInBackground(String... params) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                String urlString = getURL(params[0]);
                String response;
                try {
                    response = establishGetConnection(urlString, loadUserEmail(false), loadUserPwd(false));
                    response = response.substring(9, response.length() - 1);
                    mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd H:mm:ss.S"));
                    return mapper.readValue(response, Topic.class);
                } catch (UnsupportedEncodingException | IllegalStateException | NullPointerException e) {
                    Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
                }
            } catch (IOException e) {
                Log.e(Constants.LOG, e.getMessage());
            }
            return null;
        }

        //parameter id
        protected String getURL(String id) {
            return CONNECTION_PROTOCOL + "://" + IP + ":" + PORT + "/resources/topics/topics?id=" + id;
        }
    }

    private class GetCategoryByIdTask extends AsyncTask<String, Void, Category> {
        @Override
        protected Category doInBackground(String... params) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                String urlString = getURL(params[0]);
                String response;
                try {
                    response = establishGetConnection(urlString, loadUserEmail(false), loadUserPwd(false));
                    response = response.substring(12, response.length() - 1);
                    return mapper.readValue(response, Category.class);
                } catch (UnsupportedEncodingException | IllegalStateException | NullPointerException e) {
                    Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
                }
            } catch (IOException e) {
                Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
            }
            return null;
        }

        //parameter id
        protected String getURL(String id) {
            return CONNECTION_PROTOCOL + "://" + IP + ":" + PORT + "/resources/topics/category?id=" + id;
        }
    }

    private class GetAllTopicsTask extends AsyncTask<String, Void, List<Topic>> {
        @Override
        protected List<Topic> doInBackground(String... params) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                String urlString = getURL();
                String response;
                try {
                    response = establishGetConnection(urlString, loadUserEmail(false), loadUserPwd(false));
                    response = response.substring(10, response.length() - 1);
                    mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd H:mm:ss.S"));
                    return mapper.readValue(response,
                            TypeFactory.defaultInstance().constructCollectionType(List.class,
                                    Topic.class));
                } catch (UnsupportedEncodingException | IllegalStateException | NullPointerException e) {
                    Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
                }
            } catch (IOException e) {
                Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
            }
            return Collections.emptyList();
        }

        //all topics
        protected String getURL() {
            return CONNECTION_PROTOCOL + "://" + IP + ":" + PORT + "/resources/topics/topics/all";
        }
    }

    private class GetRolesTask extends AsyncTask<String, Void, List<Role>> {
        @Override
        protected List<Role> doInBackground(String... params) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                String urlString = getURL();
                String response;
                try {
                    response = establishGetConnection(urlString, loadUserEmail(false), loadUserPwd(false));
                    response = response.substring(9, response.length() - 1);
                    return mapper.readValue(response,
                            TypeFactory.defaultInstance().constructCollectionType(List.class,
                                    Role.class));
                } catch (UnsupportedEncodingException | IllegalStateException | NullPointerException e) {
                    Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
                }
            } catch (IOException e) {
                Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
            }
            return Collections.emptyList();
        }

        //all roles
        protected String getURL() {
            return CONNECTION_PROTOCOL + "://" + IP + ":" + PORT + "/resources/topics/roles";
        }
    }

    private class GetUserByIdTask extends AsyncTask<String, Void, User> {
        @Override
        protected User doInBackground(String... params) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                String urlString = getURL(params[0]);
                String response;
                try {
                    response = establishGetConnection(urlString, loadUserEmail(false), loadUserPwd(false));
                    response = response.substring(8, response.length() - 1);
                    mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd H:mm:ss.S"));
                    return mapper.readValue(response, User.class);
                } catch (UnsupportedEncodingException | IllegalStateException | NullPointerException e) {
                    Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
                }
            } catch (IOException e) {
                Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
            }
            return null;
        }

        //parameter id
        protected String getURL(String id) {
            return CONNECTION_PROTOCOL + "://" + IP + ":" + PORT + "/resources/topics/users?id=" + id;
        }
    }

    private class GetTopicSyncTask extends AsyncTask<String, Void, List<Id>> {
        @Override
        protected List<Id> doInBackground(String... params) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                String urlString = getURL(params[0]);
                String response;
                try {
                    String userEmail = loadUserEmail(false);
                    String userPwd = loadUserPwd(false);
                    response = establishGetConnection(urlString, userEmail, userPwd);
                    TopicsCategories topicsCategories = mapper.readValue(response, TopicsCategories.class);
                    return topicsCategories.getTopics();
                } catch (UnsupportedEncodingException | IllegalStateException | NullPointerException e) {
                    Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
                }
            } catch (IOException e) {
                Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
            }
            return Collections.emptyList();
        }

        //parameter id
        protected String getURL(String date) {
            if (date != null) {
                return CONNECTION_PROTOCOL + "://" + IP + ":" + PORT + "/resources/topics/sync?date=" + formatDate(date);
            } else {
                return CONNECTION_PROTOCOL + "://" + IP + ":" + PORT + "/resources/topics/sync";
            }

        }
    }

    private class GetCategorySyncTask extends AsyncTask<String, Void, List<Id>> {
        @Override
        protected List<Id> doInBackground(String... params) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                String urlString = getURL(params[0]);
                String response;
                try {
                    response = establishGetConnection(urlString, loadUserEmail(false), loadUserPwd(false));
                    TopicsCategories topicsCategories = mapper.readValue(response, TopicsCategories.class);
                    return topicsCategories.getCategories();
                } catch (UnsupportedEncodingException | IllegalStateException | NullPointerException e) {
                    Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
                }
            } catch (IOException e) {
                Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
            }
            return Collections.emptyList();
        }

        //parameter id
        protected String getURL(String date) {
            return CONNECTION_PROTOCOL + "://" + IP + ":" + PORT + "/resources/topics/sync?date=" + formatDate(date);
        }
    }

    private class CRUDTopicTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            return establishPostConnection(CONNECTION_PROTOCOL + "://" + IP + ":" + PORT +
                    "/resources/topics/topics", params[0], false);
        }
    }

    private class LikeDislikeTopicTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            return establishPostConnection(CONNECTION_PROTOCOL + "://" + IP + ":" + PORT +
                    "/resources/topics/userlikestopic", params[0], false);
        }
    }

    private class CRUDCategoryTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            return establishPostConnection(CONNECTION_PROTOCOL + "://" + IP + ":" + PORT +
                    "/resources/topics/category", params[0], false);
        }
    }

    private class CreateDeleteTopicCategoryTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            return establishPostConnection(CONNECTION_PROTOCOL + "://" + IP + ":" + PORT +
                    "/resources/topics/topiccategory", params[0], false);
        }
    }

    private class CRUDRoleTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            return establishPostConnection(CONNECTION_PROTOCOL + "://" + IP + ":" + PORT +
                    "/resources/topics/role", params[0], false);
        }
    }

    private class CreateDeleteUserRoleTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            return establishPostConnection(CONNECTION_PROTOCOL + "://" + IP + ":" + PORT +
                    "/resources/topics/userrole", params[0], false);
        }
    }

    private class CRUDUserTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            return establishPostConnection(CONNECTION_PROTOCOL + "://" + IP + ":" + PORT +
                    "/resources/topics/users", params[0], false);
        }
    }

    private class ActivationCodeTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            return establishPostConnection(CONNECTION_PROTOCOL + "://" + IP + ":" + PORT +
                    "/resources/topics/activationcode", params[0], true);
        }
    }

    private class AuthTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            return establishPostConnection(CONNECTION_PROTOCOL + "://" + IP + ":" + PORT +
                    "/resources/topics/auth", params[0], true);
        }
    }

    private class RegisterTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            return establishPostConnection(CONNECTION_PROTOCOL + "://" + IP + ":" + PORT +
                    "/resources/topics/register", params[0], true);
        }
    }
}