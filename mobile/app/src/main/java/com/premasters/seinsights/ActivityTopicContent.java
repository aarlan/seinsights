package com.premasters.seinsights;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.premasters.seinsights.db.Topic;
import com.premasters.seinsights.db.TopicDAO;

public class ActivityTopicContent extends ActivityNavigable {
    TopicDAO topicDAO = new TopicDAO(this);
    ActionBar mActionBar;
    String topicName, topicText, topicDate, topicCategory;
    int topicId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.topic_content);
        topicName = (String) getIntent().getExtras().get("TopicName");
        topicText = (String) getIntent().getExtras().get("TopicText");
        topicDate = (String) getIntent().getExtras().get("TopicDate");
        topicCategory = (String) getIntent().getExtras().get("TopicCategory");
        topicId = getIntent().getExtras().getInt("TopicId");
        TextView topic_category = (TextView) findViewById(R.id.topic_category);
        TextView topic_name = (TextView) findViewById(R.id.topic_name);
        TextView topic_date = (TextView) findViewById(R.id.topic_date);
        TextView textView = (TextView) findViewById(R.id.topic_content);
        topic_name.setText(topicName);
        textView.setText(topicText);
        topic_date.setText(topicDate);
        topic_category.setText(topicCategory);
        mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setTitle(topicName);
        }
        mActionBar.setDisplayHomeAsUpEnabled(true);
    }

    public void onClickDelete() {
        Topic topic_to_delete = (Topic) topicDAO.findById(topicId);
        topicDAO.delete(topic_to_delete);
        Toast.makeText(ActivityTopicContent.this, "Deleted", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(ActivityTopicContent.this, ActivityTopicList.class);
        startActivity(i);
        super.finish();
    }

    public void onClickEdit() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("ActivityTopicContent", true);
        bundle.putString("TopicName", topicName);
        Intent i = new Intent(ActivityTopicContent.this, ActivityNewTopic.class);
        i.putExtras(bundle);
        startActivity(i);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete:
                onClickDelete();
                return true;
            case R.id.edit:
                onClickEdit();
                return true;
            default:
                super.onOptionsItemSelected(item);
                return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar, menu);
        this.menu = menu;
        menu.findItem(R.id.edit).setVisible(true);
        menu.findItem(R.id.search).setVisible(false);
        menu.findItem(R.id.add).setVisible(false);
        menu.findItem(R.id.delete).setVisible(true);
        menu.findItem(R.id.more).setVisible(true);
        menu.findItem(R.id.like).setVisible(false);
        return true;
    }
}