package com.premasters.seinsights;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.FileNotFoundException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuestionnaireController {
    private static final String LOG_TAG = "QuestionException";
    private Question currentQuestion;
    private List<Question> questions;
    private List<Answer> result;
    private Answer finalScores;

    private static Context context;

    public QuestionnaireController(Context c) {
        questions = new ArrayList<>();
        result = new ArrayList<>();
        finalScores = new Answer();
        this.currentQuestion = null;
        context = c;
    }

    public QuestionnaireController() {
        questions = new ArrayList<>();
        result = new ArrayList<>();
        finalScores = new Answer();
        this.currentQuestion = null;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public List<Answer> getResults() {
        return result;
    }

    public Answer getFinalScores() {
        return finalScores;
    }

    public int getIndexOfCurrentQuestion() {
        return questions.indexOf(currentQuestion);
    }

    public void loadQuestions(String json) throws FileNotFoundException {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Question>>() {
        }.getType();
        questions = gson.fromJson(json, listType);


        if (!this.questions.isEmpty()) {
            this.currentQuestion = this.questions.get(0);
        } else
            this.currentQuestion = null;
    }

    public String storeQuestions() {

        fillQuestionsArray();
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Question>>() {
        }.getType();

        return gson.toJson(questions, listType);
    }

    public Map<String, Score> setScoresForProcesses(int sc1, int sc2, int sc3, int sc4, int sc5) {
        Map<String, Score> scores = new HashMap<>();
        scores.put("Rational Unified Process", new Score(sc1));
        scores.put("MS’s Sync & Stabilize Process", new Score(sc2));
        scores.put("Team Software Process", new Score(sc3));
        scores.put("Extreme Programming", new Score(sc4));
        scores.put("Scrum", new Score(sc5));
        return scores;
    }

    public void fillQuestionsArrayCase(String questionText, List<Answer> answers, List<String> answersText) {
        Question questionToAdd = new Question(questionText, answers, answersText);
        questions.add(questionToAdd);
    }

    public List<Answer> fillAnswers(Map<String, Score> sc1, Map<String, Score> sc2) {
        List<Answer> answers = new ArrayList<>();
        Answer answer = new Answer();
        answer.setScores(sc1);
        answers.add(answer);
        answer = new Answer();
        answer.setScores(sc2);
        answers.add(answer);
        return answers;
    }

    public List<Answer> fillAnswers(Map<String, Score> sc1, Map<String, Score> sc2, Map<String, Score> sc3) {
        List<Answer> answers = fillAnswers(sc1, sc2);
        Answer answer = new Answer();
        answer.setScores(sc3);
        answers.add(answer);
        return answers;
    }


    public List<Answer> fillAnswers(Map<String, Score> sc1, Map<String, Score> sc2, Map<String, Score> sc3, Map<String, Score> sc4) {
        List<Answer> answers = fillAnswers(sc1, sc2, sc3);
        Answer answer = new Answer();
        answer.setScores(sc4);
        answers.add(answer);
        return answers;
    }

    public List<String> fillAnswersTexts(String at1, String at2) {
        List<String> answersText = new ArrayList<>();
        answersText.add(at1);
        answersText.add(at2);
        return answersText;
    }

    public List<String> fillAnswersTexts(String at1, String at2, String at3) {
        List<String> answersText = new ArrayList<>();
        answersText.add(at1);
        answersText.add(at2);
        answersText.add(at3);
        return answersText;
    }

    public List<String> fillAnswersTexts(String at1, String at2, String at3, String at4) {
        List<String> answersText = new ArrayList<>();
        answersText.add(at1);
        answersText.add(at2);
        answersText.add(at3);
        answersText.add(at4);
        return answersText;
    }

    public void fillQuestionsArray() {
        List<Answer> answers;
        List<String> answersText;

        answers = fillAnswers(setScoresForProcesses(1, 1, 3, 3, 3), setScoresForProcesses(2, 2, 1, 1, 1), setScoresForProcesses(3, 3, 1, 1, 1));
        answersText = fillAnswersTexts("Less than 10", "10-20", "More than 20");
        fillQuestionsArrayCase("How many developers and testers,on average,are involved in a single team within the project", answers, answersText);

        answers = fillAnswers(setScoresForProcesses(2, 2, 2, 2, 2), setScoresForProcesses(3, 3, 1, 1, 2), setScoresForProcesses(3, 3, 1, 1, 2));
        answersText = fillAnswersTexts("Less than 40", "41-100", "Hundreds");
        fillQuestionsArrayCase("How many developers are involved in the entire project", answers, answersText);

        answers = fillAnswers(setScoresForProcesses(1, 1, 1, 3, 3), setScoresForProcesses(2, 2, 2, 2, 2), setScoresForProcesses(3, 3, 2, 1, 1));
        answersText = fillAnswersTexts("Hundreds to a few thousand: small to medium independent programs such as Notepad or Adobe Acrobat Reader", "Hundreds of thousands to a few million: large business applications such as Microsoft Word, Adobe Photoshop, or Rational Rose", "Millions: huge systems such as an operating system or the Space Shuttle navigation system");
        fillQuestionsArrayCase("What is the size of your product in terms of lines of code and complexity", answers, answersText);

        answers = fillAnswers(setScoresForProcesses(1, 1, 3, 1, 1), setScoresForProcesses(2, 2, 2, 1, 1), setScoresForProcesses(2, 2, 2, 2, 2), setScoresForProcesses(3, 3, 1, 3, 3));
        answersText = fillAnswersTexts("Less than 10%", "10%-25%", "25%-33%", "33%-100%");
        fillQuestionsArrayCase("What percentage of your developers are competent and experienced", answers, answersText);

        answers = fillAnswers(setScoresForProcesses(1, 3, 1, 3, 3), setScoresForProcesses(2, 2, 1, 2, 2), setScoresForProcesses(3, 2, 3, 1, 1));
        answersText = fillAnswersTexts("We've talked about the system long enough,let's just code the thing.We can always rewrite stuff that turns out to be wrong faster than if we had spent a lot of time trying to get it perfect the first time.", "We should plan and design enough of the system to feel confident that we haven't overlooked anything critical,but we should not try to get everything perfect before starting to code.", "We better make sure that we have thought through all of the possible scenarios of our system before we start writing code.Without a formal plan and design, we may make serious mistakes that will cost us later down the road.");
        fillQuestionsArrayCase("Which of the following phrases accurately describes the predominant sentiment on your team", answers, answersText);

        answers = fillAnswers(setScoresForProcesses(2, 3, 1, 3, 3), setScoresForProcesses(3, 3, 2, 2, 2), setScoresForProcesses(2, 2, 3, 1, 1));
        answersText = fillAnswersTexts("Macro-management: Only a few guidelines are outlined by the manager and developers have to fill in the gaps on their own.", "Median-management: Most of the important aspects of the project are documented by the manger,but developers still have a lot of leeway in designing and implementing the system.", "Micro-management: Almost all of the aspects of the project are documented and developers don't have much control over what they write.");
        fillQuestionsArrayCase("What kind of management style best suits your project", answers, answersText);

        answers = fillAnswers(setScoresForProcesses(3, 3, 3, 1, 1), setScoresForProcesses(2, 2, 2, 2, 2), setScoresForProcesses(2, 2, 2, 3, 3));
        answersText = fillAnswersTexts("Critical. The company wants to protect against staff turnover by retaining the knowledge acquired on each project.", "Somewhat important. Although there is occasional staff turnover, there are enough people left to retain the collective knowledge in their minds.", "Not important at all. Your organization is a small company where a high staff turnover would not matter because the company would most likely go out of business anyway.");
        fillQuestionsArrayCase("How important is it to your organization to develop organization-wide processes, rather than project-specific processes", answers, answersText);

        answers = fillAnswers(setScoresForProcesses(3, 3, 1, 2, 1), setScoresForProcesses(2, 2, 2, 2, 2));
        answersText = fillAnswersTexts("The process must be \"tried-and-true\", having been around for several years and used in a variety of market segments.", "The process is fairly mature, but has not been used in a wide variety of market segments");
        fillQuestionsArrayCase("What level of confidence do you require before adopting a new process for your project", answers, answersText);

        answers = fillAnswers(setScoresForProcesses(3, 2, 3, 1, 1), setScoresForProcesses(2, 2, 2, 2, 2), setScoresForProcesses(3, 3, 2, 2, 2), setScoresForProcesses(2, 2, 2, 2, 2));
        answersText = fillAnswersTexts("Life-critical, such as an air traffic control system or patient monitoring system", "Non life-critical, but mission-critical system, such as a stock exchange or banking application", "An embedded system that is neither life-nor mission-critical", "An application that is neither life-nor mission-critical");
        fillQuestionsArrayCase("What type of product do you have", answers, answersText);

        answers = fillAnswers(setScoresForProcesses(2, 2, 2, 3, 3), setScoresForProcesses(2, 2, 2, 2, 2));
        answersText = fillAnswersTexts("Emergent and rapidly changing", "Knowable early and largely stable");
        fillQuestionsArrayCase("How stable are the requirements from the outset of the project", answers, answersText);

        answers = fillAnswers(setScoresForProcesses(3, 2, 3, 1, 1), setScoresForProcesses(2, 2, 2, 1, 1), setScoresForProcesses(1, 2, 1, 3, 3));
        answersText = fillAnswersTexts("Extremely important. Without requirements traceability and formal documentation our project is not successful.", "Somewhat important. To ensure that we've delivered everything promised to the customer in our contract, we need to verify that all of the requirements have been traced from inception to completion", "Not really important. Requirements need to be met, but it is not important that they be traced. Formal documentation is not necessary, but does not hinder the product either.");
        fillQuestionsArrayCase("How important is requirements traceability and formal documentation ", answers, answersText);
    }

    public Question getCurrentQuestion() {
        return currentQuestion;
    }

    public int getNumberOfQuestions() {
        return questions.size();
    }

    public void getPreviousQuestion() {
        try {
            currentQuestion = questions.get(questions.indexOf(currentQuestion) - 1);
        } catch (IndexOutOfBoundsException e) {
            Log.d(LOG_TAG, "No question here. " + e);
            Toast.makeText(context,
                    "Something goes wrong. " + '\n' + "Please, try again later", Toast.LENGTH_LONG).show();
        }
    }

    public void getNextQuestion() {
        try {
            currentQuestion = questions.get(questions.indexOf(currentQuestion) + 1);
        } catch (IndexOutOfBoundsException e) {
            Log.d(LOG_TAG, "No question here. " + e);
            Toast.makeText(context,
                    "Something goes wrong. " + '\n' + "Please, try again later", Toast.LENGTH_LONG).show();
        }
    }

    public void calculateResult() {
        for (int i = 0; i < result.size(); i++) {
            List<String> keyList = new ArrayList(result.get(i).getScores().keySet());
            for (int j = 0; j < keyList.size(); j++) {
                Score s;
                if ((i == 0) || !finalScores.getScores().containsKey(keyList.get(j)))
                    s = new Score(result.get(i).getScoreForProcess(keyList.get(j)).getScoreValue());
                else {
                    s = new Score(finalScores.getScoreForProcess(keyList.get(j)).getScoreValue() +
                            result.get(i).getScoreForProcess(keyList.get(j)).getScoreValue());
                }
                finalScores.getScores().put(keyList.get(j), s);
            }
        }
    }
}