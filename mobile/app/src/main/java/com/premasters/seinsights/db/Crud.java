package com.premasters.seinsights.db;

import java.util.List;

/**
 * Created by Mtrs on 02.03.2016.
 */
public interface Crud {
    public int create(Object item);

    public int update(Object item);

    public int delete(Object item);

    public Object findById(int id);

    public List findAll();
}
