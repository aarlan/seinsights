package com.premasters.seinsights;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Timur on 21.04.2016.
 */
public class UserIdNameAndRoleId {
    private int id;
    private String name;
    private int roleid;

    @JsonCreator
    public UserIdNameAndRoleId(@JsonProperty("id") int id, @JsonProperty("name") String name, @JsonProperty("roleid") int roleid) {
        this.id = id;
        this.name = name;
        this.roleid = roleid;
    }

    public int getId() {
        return id;
    }

    public int getRoleid() {
        return roleid;
    }

    public String getName() {
        return name;
    }

}
