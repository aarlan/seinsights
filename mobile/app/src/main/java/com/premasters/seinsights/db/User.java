package com.premasters.seinsights.db;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

/**
 * Created by Alexey Merzlikin on 02.03.2016.
 */

@DatabaseTable(tableName = "Users")
public class User {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String name;

    @DatabaseField
    private String password;

    @DatabaseField
    private String email;

    @DatabaseField
    private Date created;

    @DatabaseField
    private boolean deleted;

    @DatabaseField
    private boolean activated;

    @DatabaseField
    private int activationCode;

    @DatabaseField
    private boolean ready;

    @DatabaseField
    private int roleId;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, foreignAutoCreate = true)
    private Role role;

    @ForeignCollectionField(eager = true)
    private Collection<Topic> likedTopics;

    private final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.S";

    User() {
        // all persisted classes must define a no-arg constructor with at least package visibility
    }

    public User(Role role, String name, String password, String email, String created, boolean deleted, boolean activated, int activationCode, boolean ready, int roleId) throws ParseException {
        this.role = role;
        this.name = name;
        this.password = password;
        this.email = email;
        this.created = new SimpleDateFormat(DATE_FORMAT).parse(created);
        this.deleted = deleted;
        this.activated = activated;
        this.activationCode = activationCode;
        this.ready = ready;
        this.roleId = roleId;
    }

    public Collection<Topic> getLikedTopics() {
        return likedTopics;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public int getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(int activationCode) {
        this.activationCode = activationCode;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreated() {
        return new SimpleDateFormat(DATE_FORMAT).format(created);
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }
}
