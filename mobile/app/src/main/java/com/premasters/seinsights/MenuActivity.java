package com.premasters.seinsights;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;

/**
 * Created by Timur on 24.02.2016.
 */

public class MenuActivity extends AppCompatActivity {

    SharedPreferences sPref;
    final String role = "role";
    final String name = "name";
    String userRole = "", userName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    public void onClickQuestionnaire(View view) {
        Intent i = new Intent(MenuActivity.this, ActivityQuestion.class);
        startActivity(i);
    }

    public void onClickTips(View view) {
        Intent i = new Intent(MenuActivity.this, TipsActivity.class);
        startActivity(i);
    }

    public void onClickTopics(View view) {
        try {
            loadData();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (userRole.equals("2") || userRole.equals("3")) { // If Reader or Administrator
            Toast.makeText(this, "Welcome back, " + userName + "!", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(MenuActivity.this, ActivityTopicList.class);
            startActivity(i);
        } else if (userRole.equals("1")) { // If Guest
            Intent i = new Intent(MenuActivity.this, ActivityLogin.class);
            startActivity(i);
        } else
            throw new IllegalStateException();
    }

    void loadData() throws UnsupportedEncodingException {
        sPref = getSharedPreferences("userData", MODE_PRIVATE);
        String hashedRole = sPref.getString(role, "");
        byte[] bytesRole = Base64.decode(hashedRole, Base64.DEFAULT);
        userRole = new String(bytesRole, "UTF-8");
        String hashedName = sPref.getString(name, "");
        byte[] bytesName = Base64.decode(hashedName, Base64.DEFAULT);
        userName = new String(bytesName, "UTF-8");
    }

}
