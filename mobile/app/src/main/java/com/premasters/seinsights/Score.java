package com.premasters.seinsights;

/**
 * Created by Timur on 28.01.2016.
 */

public class Score {
    private int scoreValue;

    public Score(int score) {
        this.scoreValue = score;
    }

    public int getScoreValue() {
        return scoreValue;
    }

    public void setScoreValue(int scoreValue) {
        this.scoreValue = scoreValue;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Score score1 = (Score) o;

        return scoreValue == score1.scoreValue;

    }

    @Override
    public int hashCode() {
        return scoreValue;
    }
}
