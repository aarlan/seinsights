package com.premasters.seinsights.db;

/**
 * Created by Alexey Merzlikin on 03.03.2016.
 */

import android.content.Context;
import android.util.Log;

import java.sql.SQLException;
import java.util.List;

public class RoleDAO implements Crud {

    private DatabaseHelper helper;
    private final String DAO_ERROR = "DAO_ERROR";

    public RoleDAO(Context context) {
        DatabaseManager.init(context);
        helper = DatabaseManager.getInstance().getHelper();
    }

    @Override
    public int create(Object item) {

        int index = -1;

        Role object = (Role) item;
        try {
            index = helper.getRoleDao().create(object);
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "Exception in Role DAO");
        }

        return index;
    }

    @Override
    public int update(Object item) {

        int index = -1;

        Role object = (Role) item;

        try {
            helper.getRoleDao().update(object);
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "Exception in Role DAO");
        }

        return index;
    }

    @Override
    public int delete(Object item) {

        int index = -1;

        Role object = (Role) item;

        try {
            helper.getRoleDao().delete(object);
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "Exception in Role DAO");
        }

        return index;

    }

    @Override
    public Object findById(int id) {

        Role wishList = null;
        try {
            wishList = helper.getRoleDao().queryForId(id);
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "Exception in Role DAO");
        }
        return wishList;

    }

    @Override
    public List<?> findAll() {

        List<Role> items = null;

        try {
            items = helper.getRoleDao().queryForAll();
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "Exception in Role DAO");
        }

        return items;
    }
}
