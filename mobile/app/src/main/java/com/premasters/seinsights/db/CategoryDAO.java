package com.premasters.seinsights.db;

import android.content.Context;
import android.util.Log;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Alexey Merzlikin on 03.03.2016.
 */
public class CategoryDAO implements Crud {

    private final String DAO_ERROR = "DAO_ERROR";
    private DatabaseHelper helper;

    public CategoryDAO(Context context) {
        DatabaseManager.init(context);
        helper = DatabaseManager.getInstance().getHelper();
    }

    @Override
    public int create(Object item) {

        int index = -1;

        Category object = (Category) item;
        try {
            index = helper.getCategoryDAO().create(object);
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "SQL Exception in DAO");
        }

        return index;
    }

    @Override
    public int update(Object item) {

        int index = -1;

        Category object = (Category) item;

        try {
            helper.getCategoryDAO().update(object);
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "SQL Exception in DAO");
        }

        return index;
    }

    @Override
    public int delete(Object item) {

        int index = -1;

        Category object = (Category) item;

        try {
            helper.getCategoryDAO().delete(object);
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "SQL Exception in DAO");
        }

        return index;

    }

    @Override
    public Object findById(int id) {

        Category wishList = null;
        try {
            wishList = helper.getCategoryDAO().queryForId(id);
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "SQL Exception in DAO");
        }
        return wishList;

    }

    public Object findByTitle(String title) {
        int id = -1;
        List<Category> categories = (List<Category>) this.findAll();
        for (Category c : categories) {
            if (c.getName().contains(title)) {
                id = c.getId();
            }
        }
        Category category = (Category) this.findById(id);
        return (id == -1) ? null : category;
    }

    @Override
    public List<?> findAll() {

        List<Category> items = null;

        try {
            items = helper.getCategoryDAO().queryForAll();
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "SQL Exception in DAO");
        }

        return items;
    }
}
