package com.premasters.seinsights;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;

public class ActivitySignUp extends AppCompatActivity {

    SharedPreferences sPref;
    EditText input_email, input_password, input_name;
    Button btn_signup;
    final String email = "email";
    final String password = "password";
    final String name = "name";
    NetworkController networkController;
    InternetAccessChecker IAC = new InternetAccessChecker();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_signup);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        input_email = (EditText) findViewById(R.id.input_email);
        input_password = (EditText) findViewById(R.id.input_password);
        input_name = (EditText) findViewById(R.id.input_name);

        btn_signup = (Button) findViewById(R.id.btn_signup);

        networkController = new NetworkController(getApplicationContext());

        try {
            loadData();
        } catch (UnsupportedEncodingException e) {
            Logger.getAnonymousLogger().info(e.getMessage());
        }
    }

    public void onClickSignUp(View view) throws UnsupportedEncodingException {
        saveData();

        final Activity a = ActivitySignUp.this;
        Handler h = new Handler() {
            @Override
            public void handleMessage(Message msg) {

                if (msg.what != 1) { // code if not connected
                    Toast.makeText(a, "Internet is not available at the moment. Please, try again later.", Toast.LENGTH_SHORT).show();
                } else { // code if connected
                    String response = "";

                    response = networkController.register(input_email.getText().toString(), input_password.getText().toString(),
                            input_name.getText().toString());
                    if (response.startsWith("-1.")) {
                        Toast.makeText(a, response.substring(4), Toast.LENGTH_SHORT).show();
                    } else if (response.startsWith("0.")) {
                        Toast.makeText(a, response.substring(3), Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(ActivitySignUp.this, ActivityAuthentication.class);
                        startActivity(i);
                        a.finish();
                    } else if (response.startsWith("{")) {
                        Toast.makeText(a, "Internal server error. Please, try again later.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };

        IAC.isNetworkAvailable(h, 2000, getApplicationContext());
    }

    public void onClickLogin(View view) throws UnsupportedEncodingException {
        saveData();
        Intent i = new Intent(ActivitySignUp.this, ActivityLogin.class);
        startActivity(i);
        super.finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    void saveData() throws UnsupportedEncodingException {
        sPref = getSharedPreferences("userData", MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        byte[] bytesEmail = input_email.getText().toString().getBytes("UTF-8");
        byte[] bytesPassword = input_password.getText().toString().getBytes("UTF-8");
        byte[] bytesName = input_name.getText().toString().getBytes("UTF-8");
        String hashedEmail = Base64.encodeToString(bytesEmail, Base64.DEFAULT);
        String hashedPassword = Base64.encodeToString(bytesPassword, Base64.DEFAULT);
        String hashedName = Base64.encodeToString(bytesName, Base64.DEFAULT);
        ed.putString(email, hashedEmail);
        ed.putString(password, hashedPassword);
        ed.putString(name, hashedName);
        ed.commit();
        Toast.makeText(this, "User data was successfully saved", Toast.LENGTH_SHORT).show();
    }

    void loadData() throws UnsupportedEncodingException {
        sPref = getSharedPreferences("userData", MODE_PRIVATE);
        String hashedEmail = sPref.getString(email, "");
        byte[] bytesEmail = Base64.decode(hashedEmail, Base64.DEFAULT);
        String savedEmail = new String(bytesEmail, "UTF-8");
        input_email.setText(savedEmail);
        String hashedName = sPref.getString(name, "");
        byte[] bytesName = Base64.decode(hashedName, Base64.DEFAULT);
        String savedName = new String(bytesName, "UTF-8");
        input_name.setText(savedName);
        if (!savedEmail.isEmpty() || !savedName.isEmpty())
            Toast.makeText(this, "User data was successfully loaded", Toast.LENGTH_SHORT).show();
    }
}
