package com.premasters.seinsights;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Surface;
import android.widget.Toast;

import com.premasters.seinsights.db.DatabaseManager;

import java.io.FileOutputStream;

import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "";
    QuestionnaireController questionnaireController;
    public static final String LOG_TAG = "ExceptionTag";

    SharedPreferences sPref, prefs = null;
    final String role = "role";
    final String email = "email";
    final String password = "password";
    final String name = "name";
    final String id = "id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        questionnaireController = new QuestionnaireController(getApplicationContext());
        String filename = "json";
        String string = questionnaireController.storeQuestions();

        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(string.getBytes());
            outputStream.close();
        } catch (Exception e) {
            Log.d(LOG_TAG, "Failed to open the output file. " + e);
            Toast.makeText(getApplicationContext(),
                    "Something goes wrong. " + '\n' + "Please, try again later", Toast.LENGTH_LONG).show();
        }

        forFirstRun();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("SE Insights");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(MainActivity.this, MenuActivity.class);
                startActivity(i);
                finish();
            }
        }, 3 * 1000);

        getScreenOrientation();

        //Start synchronization service
        Intent intent = new Intent(this, DatabaseSync.class);
        startService(intent);

        //Init the database manager
        DatabaseManager.init(this);
    }

    void forFirstRun() {
        prefs = getSharedPreferences("firstRun", MODE_PRIVATE);
        if (prefs.getBoolean("firstrun", true)) {
            // Do first run stuff here then set 'firstrun' as false
            try {
                String userRole = "1", userEmail = "", userPassword = "", userName = "", userId = ""; // Assigning user a Guest role
                sPref = getSharedPreferences("userData", MODE_PRIVATE);
                SharedPreferences.Editor ed = sPref.edit();
                byte[] bytesRole = userRole.getBytes("UTF-8");
                byte[] bytesEmail = userEmail.getBytes("UTF-8");
                byte[] bytesPassword = userPassword.getBytes("UTF-8");
                byte[] bytesName = userName.getBytes("UTF-8");
                byte[] bytesId = userId.getBytes("UTF-8");
                String hashedRole = Base64.encodeToString(bytesRole, Base64.DEFAULT);
                String hashedEmail = Base64.encodeToString(bytesEmail, Base64.DEFAULT);
                String hashedPassword = Base64.encodeToString(bytesPassword, Base64.DEFAULT);
                String hashedName = Base64.encodeToString(bytesName, Base64.DEFAULT);
                String hashedId = Base64.encodeToString(bytesId, Base64.DEFAULT);
                ed.putString(role, hashedRole);
                ed.putString(email, hashedEmail);
                ed.putString(password, hashedPassword);
                ed.putString(name, hashedName);
                ed.putString(id, hashedId);
                ed.commit();
            } catch (UnsupportedEncodingException e) {
                Logger.getAnonymousLogger().info(e.getMessage());
                Log.d(LOG_TAG, "Credentials check failed. " + e);
            }
            // using the following line to edit/commit prefs
            prefs.edit().putBoolean("firstrun", false).commit();
        }
    }


    private int getScreenOrientation() {
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        int orientation;
        // if the device's natural orientation is portrait:

        if ((rotation == Surface.ROTATION_0
                || rotation == Surface.ROTATION_180) && height > width) {
            orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

        } else {
            switch (rotation) {
                case Surface.ROTATION_0:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
                case Surface.ROTATION_90:
                    orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                    break;
                default:
                    Log.e(TAG, "Unknown screen orientation. Defaulting to " +
                            "landscape.");
                    orientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                    break;
            }
        }

        return orientation;
    }
}
