package com.premasters.seinsights;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


import static android.view.View.VISIBLE;

public class ActivityQuestion extends AppCompatActivity {

    private static final String LOG_TAG = "tag";
    QuestionnaireController questionnaireController;
    Button prevButton;
    Button nextButton;
    List<Integer> answeredIndex;

    RadioButton answerA;
    RadioButton answerB;
    RadioButton answerC;
    RadioButton answerD;

    View view1;
    View view2;
    View view3;
    View view4;
    View view5;

    TextView questionText;

    ActionBar mActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_view);
        questionnaireController = new QuestionnaireController(getApplicationContext());
        String questionsJSON = readFile();

        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);

        prevButton = (Button) findViewById(R.id.prev_button);
        nextButton = (Button) findViewById(R.id.next_button);

        prevButton.setVisibility(View.INVISIBLE);
        mActionBar = getSupportActionBar();
        if (mActionBar != null) mActionBar.setTitle(R.string.q_bar);
        try {
            questionnaireController.loadQuestions(questionsJSON);
        } catch (FileNotFoundException e) {
            Log.d(LOG_TAG, "Failed to load questions from JSON. " + e);
            Toast.makeText(getApplicationContext(),
                    "Something goes wrong. " + '\n' + "Please, try again later", Toast.LENGTH_LONG).show();
        }

        questionText = (TextView) findViewById(R.id.text);
        fillTextFields();

        answeredIndex = new ArrayList<>();
        for (int i = 0; i < questionnaireController.getNumberOfQuestions(); i++) {
            answeredIndex.add(-1);
        }
        deselectRadioButtons();
        nextButton.setVisibility(View.INVISIBLE);
    }

    public void setAnswersClickable(boolean b) {
        answerA.setClickable(b);
        answerB.setClickable(b);
        answerC.setClickable(b);
        answerD.setClickable(b);
    }

    public void enableNext(View view) {
        nextButton.setVisibility(View.INVISIBLE);
        RadioGroup rb = (RadioGroup) findViewById(R.id.radioGroup1);
        int checkedIndex = (rb.getCheckedRadioButtonId() - rb.getId()) / 2 - 1;
        answeredIndex.set(questionnaireController.getIndexOfCurrentQuestion(), checkedIndex);
        highlightChosenAnswer(rb);
        setAnswersClickable(false);
        final View s = view;
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Actions to do after 10 seconds
                unhighlightChosenAnswers();
                nextButton.setVisibility(View.VISIBLE);
                setAnswersClickable(true);
                onClickNext(s);
            }
        }, 300);
    }

    public void onClickNext(View view) {
        unhighlightChosenAnswers();
        RadioGroup rb = (RadioGroup) findViewById(R.id.radioGroup1);
        if (prevButton.getVisibility() == View.INVISIBLE) {
            prevButton.setVisibility(VISIBLE);
        }

        if (questionnaireController.getIndexOfCurrentQuestion() == questionnaireController.getNumberOfQuestions() - 1) {
            nextButton.setVisibility(View.INVISIBLE);
            for (int i = 0; i < questionnaireController.getNumberOfQuestions(); i++) {
                questionnaireController.getResults().add(questionnaireController.getQuestions().get(i).getAnswers().get(answeredIndex.get(i)));
            }
            questionnaireController.calculateResult();
            // go to ActivityAnswers
            Intent intent = new Intent(ActivityQuestion.this, ActivityAnswers.class);
            List<String> res = new ArrayList<>(questionnaireController.getFinalScores().getScores().keySet());
            String[] processes = new String[questionnaireController.getFinalScores().getScores().size()];
            int[] scores = new int[questionnaireController.getFinalScores().getScores().size()];
            for (int i = 0; i < questionnaireController.getFinalScores().getScores().size(); i++) {
                processes[i] = res.get(i);
                scores[i] = questionnaireController.getFinalScores().getScores().get(res.get(i)).getScoreValue();
            }
            intent.putExtra("processes", processes);
            intent.putExtra("scores", scores);
            startActivity(intent);
            super.finish();
        } else {
            questionnaireController.getNextQuestion();

            nextButton.setBackgroundResource(R.drawable.butright);
            if (questionnaireController.getIndexOfCurrentQuestion() == questionnaireController.getNumberOfQuestions() - 1) {
            } else {
                nextButton.setBackgroundResource(R.drawable.butright);
            }
            if (answeredIndex.get(questionnaireController.getIndexOfCurrentQuestion()) == -1) {
                deselectRadioButtons();
                nextButton.setVisibility(View.INVISIBLE);
            } else {
                rb.check(answeredIndex.get(questionnaireController.getIndexOfCurrentQuestion()));
                if (rb.getCheckedRadioButtonId() >= 0) {
                    nextButton.setVisibility(VISIBLE);
                }
            }
            fillTextFields();
            highlightChosenAnswer(rb);
        }
    }

    public void onClickPrev(View view) {
        RadioGroup rb = (RadioGroup) findViewById(R.id.radioGroup1);
        questionnaireController.getPreviousQuestion();
        if (questionnaireController.getIndexOfCurrentQuestion() == 0) {
            prevButton.setVisibility(View.INVISIBLE);
        }
        rb.check(answeredIndex.get(questionnaireController.getIndexOfCurrentQuestion()));
        if (rb.getCheckedRadioButtonId() >= 0) {
            nextButton.setVisibility(VISIBLE);
            nextButton.setBackgroundResource(R.drawable.butright);
        }
        fillTextFields();
        highlightChosenAnswer(rb);
    }

    private void setRadioButtonImagesUnchecked() {
        answerA.setButtonDrawable(R.drawable.uncheck);
        answerB.setButtonDrawable(R.drawable.uncheck);
        answerC.setButtonDrawable(R.drawable.uncheck);
        answerD.setButtonDrawable(R.drawable.uncheck);
    }

    private void unhighlightChosenAnswers() {
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.questionRelativeLayout);
        answerA.setBackgroundColor(rl.getDrawingCacheBackgroundColor());
        answerB.setBackgroundColor(rl.getDrawingCacheBackgroundColor());
        answerC.setBackgroundColor(rl.getDrawingCacheBackgroundColor());
        answerD.setBackgroundColor(rl.getDrawingCacheBackgroundColor());
    }

    private void highlightChosenAnswer(RadioGroup rb) {
        if (answeredIndex.get(questionnaireController.getIndexOfCurrentQuestion()) >= 0) {
            setRadioButtonImagesUnchecked();
            unhighlightChosenAnswers();
            switch (answeredIndex.get(questionnaireController.getIndexOfCurrentQuestion())) {
                case 0:
                    answerA.setButtonDrawable(R.drawable.check);
                    answerA.setBackgroundColor(Color.parseColor("#CFD8DC"));
                    break;
                case 1:
                    answerB.setButtonDrawable(R.drawable.check);
                    answerB.setBackgroundColor(Color.parseColor("#CFD8DC"));
                    break;
                case 2:
                    answerC.setButtonDrawable(R.drawable.check);
                    answerC.setBackgroundColor(Color.parseColor("#CFD8DC"));
                    break;
                case 3:
                    answerD.setButtonDrawable(R.drawable.check);
                    answerD.setBackgroundColor(Color.parseColor("#CFD8DC"));
                    break;
                default:
                    break;
            }
        }

    }

    private void deselectRadioButtons() {
        RadioGroup rb = (RadioGroup) findViewById(R.id.radioGroup1);
        rb.clearCheck();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void fillTextFields() {
        int numberOfAnswers = questionnaireController.getCurrentQuestion().getAnswersText().size();
        questionText.setText((questionnaireController.getIndexOfCurrentQuestion() + 1) + "/" + questionnaireController.getQuestions().size() + " " + questionnaireController.getCurrentQuestion().getQuestionText() + " ?");

        answerA = (RadioButton) findViewById(R.id.answerA);
        answerB = (RadioButton) findViewById(R.id.answerB);
        answerC = (RadioButton) findViewById(R.id.answerC);
        answerD = (RadioButton) findViewById(R.id.answerD);

        view1 = findViewById(R.id.view1);
        view2 = findViewById(R.id.view2);
        view3 = findViewById(R.id.view3);
        view4 = findViewById(R.id.view4);
        view5 = findViewById(R.id.view5);

        answerA.setVisibility(View.INVISIBLE);
        answerB.setVisibility(View.INVISIBLE);
        answerC.setVisibility(View.INVISIBLE);
        answerD.setVisibility(View.INVISIBLE);

        view1.setVisibility(View.INVISIBLE);
        view2.setVisibility(View.INVISIBLE);
        view3.setVisibility(View.INVISIBLE);
        view4.setVisibility(View.INVISIBLE);
        view5.setVisibility(View.INVISIBLE);

        switch (numberOfAnswers) {
            case 2:
                twoVariants();
                break;
            case 3:
                threeVariants();
                break;
            case 4:
                fourVariants();
                break;
            default:
                Log.d(LOG_TAG, "Unlikely to get there");
                break;
        }
    }

    private void twoVariants() {
        view1.setVisibility(VISIBLE);
        answerA.setVisibility(VISIBLE);
        answerA.setButtonDrawable(R.drawable.uncheck);
        answerA.setText(questionnaireController.getCurrentQuestion().getAnswerText(0));
        view2.setVisibility(VISIBLE);
        answerB.setVisibility(VISIBLE);
        answerB.setButtonDrawable(R.drawable.uncheck);
        answerB.setText(questionnaireController.getCurrentQuestion().getAnswerText(1));
        view3.setVisibility(VISIBLE);
    }

    public void threeVariants() {
        twoVariants();
        answerC.setVisibility(VISIBLE);
        answerC.setButtonDrawable(R.drawable.uncheck);
        answerC.setText(questionnaireController.getCurrentQuestion().getAnswerText(2));
        view4.setVisibility(VISIBLE);
    }

    public void fourVariants() {
        threeVariants();
        answerD.setVisibility(VISIBLE);
        answerD.setButtonDrawable(R.drawable.uncheck);
        answerD.setText(questionnaireController.getCurrentQuestion().getAnswerText(3));
        view5.setVisibility(VISIBLE);
    }

    private String readFile() {
        FileInputStream stream;
        StringBuilder sb = new StringBuilder();
        String line, json = "";

        try {
            stream = openFileInput("json");

            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } finally {
                stream.close();
            }

            json = sb.toString();

        } catch (Exception e) {
            Log.d(LOG_TAG, "Failed to open JSON file. " + e);
            Toast.makeText(getApplicationContext(),
                    "Something goes wrong. " + '\n' + "Please, try again later", Toast.LENGTH_LONG).show();
        }
        return json;
    }
}
