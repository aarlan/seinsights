package com.premasters.seinsights;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.net.Socket;

/**
 * Created by Timur on 24.02.2016.
 */

public class TipsActivity extends AppCompatActivity {

    Socket socket;
    String requestString;
    String responseString;

    NetworkController networkController;

    EditText requestText;
    TextView responseText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips);
        requestText = (EditText) findViewById(R.id.editText);
        responseText = (TextView) findViewById(R.id.textView10);
        networkController = new NetworkController(getApplicationContext());
    }

    //just to test functionality
    public void onClickSend(View view) {
//        Topic topic = networkController.getTopicById("1");
//        responseText.setText(topic.getText());
//        User user = networkController.getUserById("1");
//        responseText.setText(user.getName());
//        List<Topic> topicList;
//        topicList = networkController.getAllTopics();
//        responseText.setText(topicList.get(0).getText());
//        List<Role> roleList;
//        roleList = networkController.getRoles();
//       responseText.setText(roleList.get(0).getName());
//        List<Category> categoryList;
//        categoryList = networkController.getNewCategories("2015-07-19%2001%3A23%3A45.6");
//        responseText.setText(categoryList.get(0).getName());
//        String response = networkController.createCategory("Test1");
//        responseText.setText(response);

    }
}
