package com.premasters.seinsights;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;

public abstract class ActivityNavigable extends AppCompatActivity {
    Menu menu;
    SharedPreferences sPref;
    final String role = "role";

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.search:
                for (int i = 0; i < menu.size(); i++) {
                    if (menu.getItem(i).getItemId() != R.id.search) {
                        menu.getItem(i).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
                    }
                }
                break;
            case R.id.like:
                Toast.makeText(ActivityNavigable.this, "Show favourite!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.add:
                Intent i_add = new Intent(ActivityNavigable.this, ActivityNewTopic.class);
                startActivity(i_add);
                super.finish();
                break;
            case R.id.edit:
                break;
            case R.id.delete:
                break;
            case R.id.share:
                Toast.makeText(ActivityNavigable.this, "Share via ", Toast.LENGTH_SHORT).show();
                break;
            case R.id.settings:
                Intent i_set = new Intent(ActivityNavigable.this, ActivitySettings.class);
                startActivity(i_set);
                super.finish();
                break;
            case R.id.exit:
                try {
                    signOut();
                } catch (UnsupportedEncodingException e) {
                    Log.e(Constants.LOG, e.getMessage(), e.fillInStackTrace());
                }
                break;
            default:
                break;
        }
        return true;
    }

    void signOut() throws UnsupportedEncodingException {
        sPref = getSharedPreferences("userData", MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        byte[] bytesRole = "1".getBytes("UTF-8");
        String hashedRole = Base64.encodeToString(bytesRole, Base64.DEFAULT);
        ed.putString(role, hashedRole);
        ed.commit();
        Intent i = new Intent(ActivityNavigable.this, MenuActivity.class);
        startActivity(i);
        finish();
        Toast.makeText(this, "You have successfully signed out.", Toast.LENGTH_SHORT).show();
    }
}
