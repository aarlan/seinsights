package com.premasters.seinsights.db;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Collection;

/**
 * Created by Merzlikin Alexey on 03.03.2016.
 */

@DatabaseTable(tableName = "Categories")
public class Category {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String name;

    @ForeignCollectionField(eager = false)
    Collection<Topic> topics;

    Category() {
        // all persisted classes must define a no-arg constructor with at least package visibility
    }

    public Category(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<Topic> getTopics() {
        return topics;
    }
}
