package com.premasters.seinsights;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.premasters.seinsights.db.Category;
import com.premasters.seinsights.db.CategoryDAO;
import com.premasters.seinsights.db.Topic;
import com.premasters.seinsights.db.TopicDAO;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Ruz on 03.03.2016.
 */
public class ActivityNewTopic extends ActivityNavigable {
    TopicDAO topicDAO = new TopicDAO(this);

    ActionBar mActionBar;
    TextView date;
    EditText edit_cat, edit_title, edit_text;
    int topicId, categoryId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_topic);
        mActionBar = getSupportActionBar();
        date = (TextView) findViewById(R.id.date);
        edit_cat = (EditText) findViewById(R.id.edit_category);
        edit_title = (EditText) findViewById(R.id.edit_title);
        edit_text = (EditText) findViewById(R.id.edit_text);
        if (getIntent().getExtras() != null && getIntent().getExtras().get("ActivityTopicContent") != null && getIntent().getExtras().getBoolean("ActivityTopicContent", false)) {
            Topic topic = (Topic) topicDAO.findByTitle((String) getIntent().getExtras().get("TopicName"));
            topicId = topic.getId();
            edit_title.setText(topic.getTitle());
            if (topic.getCategory() != null) {
                categoryId = topic.getCategory().getId();
                edit_cat.setText(topic.getCategory().getName());
            }
            edit_text.setText(topic.getText());
            if (mActionBar != null) {
                mActionBar.setTitle("Edit topic");
                mActionBar.setDisplayHomeAsUpEnabled(true);
            }
        } else {
            if (mActionBar != null) {
                mActionBar.setTitle("New topic");
                mActionBar.setDisplayHomeAsUpEnabled(true);
            }
        }
    }


    public void onClickCreate(View view) throws ParseException {
        Date date = new Date();
        String data_str = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(date);
        CategoryDAO categoryDAO = new CategoryDAO(this);
        String cat = edit_cat.getText().toString();
        String title = edit_title.getText().toString();
        String text = edit_text.getText().toString();
        Topic topic;
        if (mActionBar != null && mActionBar.getTitle().equals("Edit topic")) {
            topic = (Topic) topicDAO.findById(topicId);
            if (categoryId != -1) {
                Category category = (Category) categoryDAO.findById(categoryId);
                category.setName(cat);
                categoryDAO.update(category);
                topic.setCategory(category);
            } else {
                topic.setCategory(new Category(cat));
            }
            topic.setTitle(title);
            topic.setText(text);
            topicDAO.update(topic);
            Toast.makeText(ActivityNewTopic.this, "Edited", Toast.LENGTH_SHORT).show();
        } else {
            categoryDAO.create(new Category(cat));
            topic = new Topic((Category) null,
                    text,
                    title,
                    0,
                    data_str,
                    data_str,
                    false, true);
            topicDAO.create(topic);
            Toast.makeText(ActivityNewTopic.this, "Created", Toast.LENGTH_SHORT).show();
        }
        TemporaryStorage temporaryStorage = new TemporaryStorage(getApplicationContext());
        temporaryStorage.saveTopicId(topic.getId());
        Intent i = new Intent(ActivityNewTopic.this, ActivityTopicList.class);
        startActivity(i);
        super.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar, menu);
        this.menu = menu;
        menu.findItem(R.id.search).setVisible(false);
        menu.findItem(R.id.add).setVisible(false);
        menu.findItem(R.id.delete).setVisible(false);
        menu.findItem(R.id.edit).setVisible(false);
        menu.findItem(R.id.more).setVisible(true);
        return true;
    }


}