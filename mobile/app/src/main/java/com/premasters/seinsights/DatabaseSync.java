package com.premasters.seinsights;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.premasters.seinsights.db.Category;
import com.premasters.seinsights.db.CategoryDAO;
import com.premasters.seinsights.db.Topic;
import com.premasters.seinsights.db.TopicDAO;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Alexey Merzlikin on 08.03.2016.
 */
public class DatabaseSync extends IntentService {

    private final String SYNC_ERROR = "SYNC_ERROR";
    private NetworkController networkController;
    private TemporaryStorage temporaryStorage;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public DatabaseSync() {
        super("DatabaseSync");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        while (true) {
            networkController = new NetworkController(this);
            temporaryStorage = new TemporaryStorage(this);
            if (networkController.checkConnection(this)) {
                performSyncWithServer();
                try {
                    performSyncWithMobile();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            networkController = null;
            temporaryStorage = null;
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                // Restore interrupt status.
                Thread.currentThread().interrupt();
            }
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    private void performSyncWithServer() {
        List<Topic> topics = (networkController.getNewTopics(this.getLastTopic()) != null) ?
                networkController.getNewTopics(this.getLastTopic()) : null;
        if (topics != null) {
            try {
                this.updateTopics(topics);
            } catch (ParseException e) {
                Log.d(SYNC_ERROR, Arrays.toString(e.getStackTrace()));
                e.printStackTrace();
            }
        }
    }

    private void performSyncWithMobile() throws UnsupportedEncodingException {
        //Check if something is needed to send from mobile to server
        List<Topic> topics = getTopicsInTemporaryStorage();
        //Check if something is sent
        if (!topics.isEmpty() && networkController.syncTopics(this, topics)) {
            //Clear local storage to synchronize
            temporaryStorage.clearStorage();
        }
    }

    private List<Topic> getTopicsInTemporaryStorage() {
        return temporaryStorage.getTopicsToSend();
    }

    private String getLastTopic() {
        TopicDAO topicDAO = new TopicDAO(this);
        return topicDAO.findDateOfLastTopic();
    }

    public void updateTopics(List<Topic> newData) throws ParseException {
        if (newData != null) {
            TopicDAO topicDAO = new TopicDAO(this);
            List<Topic> topics = newData;
            for (Topic topic : topics) {
                topicDAO.create(new Topic(topic.getCategory(),
                        topic.getText(),
                        topic.getTitle(),
                        topic.getLikes(),
                        topic.getCreated(),
                        topic.getModified(),
                        topic.isDeleted(),
                        topic.isReady()));
            }
        }

    }

    public <V> void updateDatabase(List<V> newData) throws ParseException {
        if (newData.get(0) instanceof Topic) {
            TopicDAO topicDAO = new TopicDAO(this);
            List<Topic> topics = (List<Topic>) newData;
            for (Topic topic : topics) {
                topicDAO.create(new Topic(topic.getCategory(),
                        topic.getText(),
                        topic.getTitle(),
                        topic.getLikes(),
                        topic.getCreated(),
                        topic.getModified(),
                        topic.isDeleted(),
                        topic.isReady()));

                System.out.println("TEXT = " + topic.getText());
            }
        } else if (newData.get(0) instanceof Category) {
            CategoryDAO categoryDAO = new CategoryDAO(this);
            List<Category> categories = (List<Category>) newData;
            for (Category category : categories) {
                categoryDAO.create(new Category(category.getName()));
            }

        }
    }

}
