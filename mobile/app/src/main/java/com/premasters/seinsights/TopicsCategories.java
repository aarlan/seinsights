package com.premasters.seinsights;

import java.util.List;

/**
 * Created by Timur on 17.03.2016.
 */
public class TopicsCategories {

    private List<Id> topics;

    private List<Id> categories;

    public TopicsCategories() {
    }

    public TopicsCategories(List<Id> topics, List<Id> categories) {
        this.topics = topics;
        this.categories = categories;
    }

    public List<Id> getCategories() {
        return categories;
    }

    public void setCategories(List<Id> categories) {
        this.categories = categories;
    }

    public List<Id> getTopics() {
        return topics;
    }

    public void setTopics(List<Id> topics) {
        this.topics = topics;
    }
}
