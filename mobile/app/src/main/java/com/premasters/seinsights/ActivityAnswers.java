package com.premasters.seinsights;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Aleksandr Nedorezov on 2/4/16.
 */

public class ActivityAnswers extends AppCompatActivity {

    String[] processes;
    WebView wv;
    Map<String, String> links;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Your Results");
        actionBar.setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.answer_view);

        List<TextView> processNames = new ArrayList<>();
        processNames.add((TextView) findViewById(R.id.process1));
        processNames.add((TextView) findViewById(R.id.process2));
        processNames.add((TextView) findViewById(R.id.process3));
        processNames.add((TextView) findViewById(R.id.process4));
        processNames.add((TextView) findViewById(R.id.process5));

        List<TextView> processScores = new ArrayList<>();
        processScores.add((TextView) findViewById(R.id.answA));
        processScores.add((TextView) findViewById(R.id.answB));
        processScores.add((TextView) findViewById(R.id.answC));
        processScores.add((TextView) findViewById(R.id.answD));
        processScores.add((TextView) findViewById(R.id.answE));

        wv = (WebView) findViewById(R.id.webView);
        wv.setVisibility(View.INVISIBLE);

        links = new HashMap<>();
        links.put("Rational Unified Process", getString(R.string.RUP_LINK));
        links.put("Microsoft’s Sync & Stabilize Process", getString(R.string.MSS_LINK));
        links.put("Team Software Process", getString(R.string.TSP_LINK));
        links.put("Extreme Programming", getString(R.string.XP_LINK));
        links.put("Scrum", getString(R.string.SCRUM_LINK));

        processes = getIntent().getStringArrayExtra("processes");
        int[] scores = getIntent().getIntArrayExtra("scores");

        quicksort(scores, processes, 0, scores.length - 1);

        reverse(processes);
        reverse(scores);

        for (int i = 0; i < processes.length; i++) {
            processNames.get(i).setText(processes[i]);
            processScores.get(i).setText(String.valueOf(scores[i]));
        }
        for (int i = processes.length; i < processNames.size(); i++) {
            processNames.get(i).setVisibility(View.INVISIBLE);
            processScores.get(i).setVisibility(View.INVISIBLE);
        }
    }

    public void openProcessDescription1(View view) {
        wv.loadUrl(links.get(processes[0]));
    }

    public void openProcessDescription2(View view) {
        wv.loadUrl(links.get(processes[1]));
    }

    public void openProcessDescription3(View view) {
        wv.loadUrl(links.get(processes[2]));
    }

    public void openProcessDescription4(View view) {
        wv.loadUrl(links.get(processes[3]));
    }

    public void openProcessDescription5(View view) {
        wv.loadUrl(links.get(processes[4]));
    }

    public void startNew(View view) {
        Intent i = new Intent(ActivityAnswers.this, ActivityQuestion.class);
        startActivity(i);
        super.finish();
    }

    static int partition(int[] array, String[] arrS, int start, int end) {
        int marker = start;
        for (int i = start; i <= end; i++) {
            if (array[i] <= array[end]) {
                int temp = array[marker]; // swap
                String tempS = arrS[marker];
                array[marker] = array[i];
                arrS[marker] = arrS[i];
                array[i] = temp;
                arrS[i] = tempS;
                marker += 1;
            }
        }
        return marker - 1;
    }

    static void quicksort(int[] array, String[] arrS, int start, int end) {
        if (start >= end) {
            return;
        }
        int pivot = partition(array, arrS, start, end);
        quicksort(array, arrS, start, pivot - 1);
        quicksort(array, arrS, pivot + 1, end);
    }

    protected <K extends Comparable<K>> void swap(K[] array, int i, int j) {
        K temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    public <K extends Comparable<K>> void reverse(K[] array) {
        int start = 0;
        int beyond = array.length;
        while (start != beyond) {
            --beyond;
            if (start != beyond) {
                swap(array, start, beyond);
                ++start;
            }
        }
    }

    protected void swap(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    public void reverse(int[] array) {
        int start = 0;
        int beyond = array.length;
        while (start != beyond) {
            --beyond;
            if (start != beyond) {
                swap(array, start, beyond);
                ++start;
            }
        }
    }
}
