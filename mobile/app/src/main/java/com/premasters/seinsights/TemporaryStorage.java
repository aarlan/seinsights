package com.premasters.seinsights;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.premasters.seinsights.db.Topic;
import com.premasters.seinsights.db.TopicDAO;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Alexey Merzlikin on 24.03.2016.
 */
public class TemporaryStorage {

    public static final String STORAGE_NAME = "topicsToSend";

    private SharedPreferences sharedPreferences;
    private Editor editor;
    private Context context;

    public TemporaryStorage(Context context) {
        this.context = context;
        this.sharedPreferences = context.getSharedPreferences(STORAGE_NAME, context.MODE_PRIVATE);
    }

    public void saveTopicId(Integer id) {
        Set<String> idSet;
        if (this.sharedPreferences.getStringSet(STORAGE_NAME, null) != null) {
            idSet = this.sharedPreferences.getStringSet(STORAGE_NAME, null);
        } else {
            idSet = new HashSet<>();
        }
        idSet.add(id.toString());
        editor = sharedPreferences.edit();
        editor.putStringSet(STORAGE_NAME, idSet);
        editor.apply();
    }

    private Set<String> getTopicIdsToSendToServer() {
        return sharedPreferences.getStringSet(STORAGE_NAME, null);
    }

    public List<Topic> getTopicsToSend() {
        Set<String> set = getTopicIdsToSendToServer();
        if (set != null) {
            List<Topic> topics = new ArrayList<>();
            TopicDAO topicDAO = new TopicDAO(context);
            for (String id : set) {
                Topic topic = (Topic) topicDAO.findById(Integer.parseInt(id));
                topics.add(topic);
            }
            return topics;
        } else {
            return new ArrayList<Topic>();
        }
    }

    public void clearStorage() {
        editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}
