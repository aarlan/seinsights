package com.premasters.seinsights.db;

import android.content.Context;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexey Merzlikin on 03.03.2016.
 */
public class TopicDAO implements Crud {

    private DatabaseHelper helper;
    private final String DAO_ERROR = "DAO_ERROR";

    public TopicDAO(Context context) {
        DatabaseManager.init(context);
        helper = DatabaseManager.getInstance().getHelper();
    }

    @Override
    public int create(Object item) {

        int index = -1;

        Topic object = (Topic) item;
        try {
            index = helper.getTopicDao().create(object);
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "SQL Exception in Topic DAO");
        }

        return index;
    }

    @Override
    public int update(Object item) {

        int index = -1;

        Topic object = (Topic) item;

        try {
            helper.getTopicDao().update(object);
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "SQL Exception in Topic DAO");
        }

        return index;
    }

    @Override
    public int delete(Object item) {

        int index = -1;

        Topic object = (Topic) item;

        try {
            helper.getTopicDao().delete(object);
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "SQL Exception in Topic DAO");
        }

        return index;

    }

    @Override
    public Object findById(int id) {

        Topic wishList = null;
        try {
            wishList = helper.getTopicDao().queryForId(id);
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "SQL Exception in Topic DAO");
        }
        return wishList;

    }

    public Object findByTitle(String title) {
        int id = -1;
        List<Topic> topics = (List<Topic>) this.findAll();
        for (Topic t : topics) {
            if (t.getTitle().contains(title)) {
                id = t.getId();
            }
        }
        Topic topic = (Topic) this.findById(id);
        return (id == -1) ? null : topic;
    }

    public String findDateOfLastTopic() {
        List<Topic> topics = (List<Topic>) this.findAll();
        int id;
        if (topics.isEmpty())
            return null;
        id = topics.size() - 1;
        Topic topic = (Topic) this.findById(id);
        if (topic == null)
            id = -1;
        return (id == -1) ? null : topic.getModified();
    }

    public void addLike(Topic topic) {
        topic.setLikes(topic.getLikes() + 1);
    }

    public void removeLike(Topic topic) {
        topic.setLikes(topic.getLikes() - 1);
    }

    @Override
    public List<?> findAll() {

        List<Topic> items = new ArrayList<>();

        try {
            items = helper.getTopicDao().queryForAll();
        } catch (SQLException e) {
            Log.d(DAO_ERROR, "SQL Exception in Topic DAO");
        }

        return items;
    }
}
