package com.premasters.seinsights;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Timur on 28.01.2016.
 * Updated by Aleksandr Nedorezov on 29.01.2016.
 */

public class Question {

    private String questionText, comment;
    private List<Answer> answers = new ArrayList<>();
    private List<String> answersText = new ArrayList<>();

    public Question(String questionText, List<Answer> answers, List<String> answersText) {
        this.questionText = questionText;
        this.answers = answers;
        this.answersText = answersText;
    }


    public String getQuestionText() {

        return questionText;
    }

    public String getCommentText() {

        return comment;
    }

    public List<Answer> getAnswers() {
        return answers;
    }


    public List<String> getAnswersText() {
        return answersText;
    }

    public String getAnswerText(int i) {
        return answersText.get(i);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Question question = (Question) o;

        if (questionText.equals(question.questionText)
                && answers.equals(question.answers)
                && answersText.equals(question.answersText))
            return true;
        return false;

    }

    @Override
    public int hashCode() {
        int result = questionText != null ? questionText.hashCode() : 0;
        result = 31 * result + (answers != null ? answers.hashCode() : 0);
        result = 31 * result + (answersText != null ? answersText.hashCode() : 0);
        return result;
    }
}