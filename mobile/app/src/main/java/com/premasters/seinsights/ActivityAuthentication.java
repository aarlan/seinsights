package com.premasters.seinsights;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;

public class ActivityAuthentication extends AppCompatActivity {

    SharedPreferences sPref;
    NetworkController networkController;
    final String email = "email";
    final String password = "password";
    String userEmail = "", userPassword = "";
    Button submitButton;
    EditText input_activationCode;
    InternetAccessChecker IAC = new InternetAccessChecker();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_authentication);
        networkController = new NetworkController(getApplicationContext());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        submitButton = (Button) findViewById(R.id.btn_confirm);

        input_activationCode = (EditText) findViewById(R.id.input_code);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    public void onClickSubmit(View view) throws UnsupportedEncodingException {
        submitButton.setEnabled(false);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Actions to do after 2 seconds
                submitButton.setEnabled(true);
            }
        }, 2000);


        final Activity a = ActivityAuthentication.this;
        Handler h = new Handler() {
            @Override
            public void handleMessage(Message msg) {

                if (msg.what != 1) { // code if not connected
                    Toast.makeText(a, "Internet is not available at the moment. Please, try again later.", Toast.LENGTH_SHORT).show();
                } else { // code if connected
                    try {
                        loadData();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    String response = networkController.sendActivationCode(userEmail, userPassword, input_activationCode.getText().toString());
                    if (response.startsWith("-1.")) {
                        input_activationCode.setText("");
                        if (response.startsWith("-1. 403."))
                            Toast.makeText(a, response.substring(9), Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(a, response.substring(4), Toast.LENGTH_SHORT).show();
                    } else if (response.startsWith("0.")) {
                        Toast.makeText(a, response.substring(3), Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(ActivityAuthentication.this, ActivityLogin.class);
                        startActivity(i);
                        finish();
                    } else if (response.startsWith("{")) {
                        Toast.makeText(a, "Internal server error. Please, try again later.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };

        IAC.isNetworkAvailable(h, 2000, getApplicationContext());
    }

    void loadData() throws UnsupportedEncodingException {
        sPref = getSharedPreferences("userData", MODE_PRIVATE);
        String hashedEmail = sPref.getString(email, "");
        byte[] bytesEmail = Base64.decode(hashedEmail, Base64.DEFAULT);
        userEmail = new String(bytesEmail, "UTF-8");
        String hashedPassword = sPref.getString(password, "");
        byte[] bytesPassword = Base64.decode(hashedPassword, Base64.DEFAULT);
        userPassword = new String(bytesPassword, "UTF-8");
    }

}
