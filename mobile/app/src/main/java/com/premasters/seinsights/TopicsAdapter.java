package com.premasters.seinsights;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.premasters.seinsights.db.Topic;

import java.util.List;

/**
 * Created by Ruz on 17.03.2016.
 */
public class TopicsAdapter extends BaseAdapter {

    List<Topic> topics;
    private Activity activity;
    private LayoutInflater lInflater;

    public TopicsAdapter(List<Topic> topics, Activity activity) {
        this.topics = topics;
        this.activity = activity;
        lInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return topics.size();
    }

    @Override
    public Object getItem(int position) {
        return topics.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.single_topic, parent, false);
        }
        final Topic topic = topics.get(position);
        final TextView topicName = (TextView) view.findViewById(R.id.topic_name);
        final TextView topicDate = (TextView) view.findViewById(R.id.topic_date);
        final CheckBox like = (CheckBox) view.findViewById(R.id.like);
        final String text = topic.getText();
        topicName.setText(topic.getTitle());
        topicDate.setText(topic.getCreated().substring(0, 10) + " ");
        like.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //TODO: Put checked variable into database
                if (isChecked) {
                    buttonView.setButtonDrawable(R.drawable.heart);
                } else {
                    buttonView.setButtonDrawable(R.drawable.noheart);
                }
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("TopicId", topic.getId());
                bundle.putString("TopicName", (String) topicName.getText());
                bundle.putString("TopicText", text);
                bundle.putString("TopicDate", (String) topicDate.getText());
                if (topic.getCategory() != null) {
                    bundle.putString("TopicCategory", topic.getCategory().getName());
                } else {
                    bundle.putString("TopicCategory", "");
                }
                Intent i = new Intent(activity, ActivityTopicContent.class);
                i.putExtras(bundle);
                activity.startActivity(i);
            }
        });
        return view;
    }
}
