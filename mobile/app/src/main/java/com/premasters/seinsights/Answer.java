package com.premasters.seinsights;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Timur on 28.01.2016.
 * Modified by Aleksandr Nedorezov on 05.02.2016.
 */

public class Answer {

    private Map<String, Score> scores;

    public Answer() {
        this.scores = new HashMap();
    }


    public Answer(String process, Score score) {
        this.scores = new HashMap();
        this.scores.put(process, score);
    }

    public Map<String, Score> getScores() {
        return scores;
    }

    public void setScores(Map<String, Score> scores) {
        this.scores = scores;
    }

    public Score getScoreForProcess(String p) {
        return scores.get(p);
    }

    public void setScoreForProcess(String p, int s) {
        scores.get(p).setScoreValue(s);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Answer answer = (Answer) o;
        return scores.equals(answer.scores);

    }

    @Override
    public int hashCode() {
        return scores != null ? scores.hashCode() : 0;
    }
}
