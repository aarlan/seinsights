package com.premasters.seinsights;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.premasters.seinsights.db.Topic;
import com.premasters.seinsights.db.TopicDAO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ruz on 03.03.2016.
 */
public class ActivityTopicList extends ActivityNavigable {
    ActionBar mActionBar;
    List<Topic> topics;
    TopicDAO topicDAO;
    ListView topicNames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        topicDAO = new TopicDAO(this);
        topics = (List<Topic>) topicDAO.findAll();
        List<String> topicNamesList = new ArrayList<>();
        Integer g = topics.size();
        Log.d("DB", g.toString());
        for (Topic topic : topics) {
            topicNamesList.add(topic.getTitle());
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.topic_list);
        topicNames = (ListView) (findViewById(R.id.topic_name));
        topicNames.setAdapter(new TopicsAdapter(topics, ActivityTopicList.this));

        mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setTitle("All topics");
        }
        mActionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        topics = (List<Topic>) topicDAO.findAll();
        topicNames.setAdapter(new TopicsAdapter(topics, ActivityTopicList.this));
        topicNames.invalidate();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar, menu);
        this.menu = menu;
        menu.findItem(R.id.search).setVisible(true);
        menu.findItem(R.id.add).setVisible(true);
        menu.findItem(R.id.delete).setVisible(false);
        menu.findItem(R.id.edit).setVisible(false);
        menu.findItem(R.id.more).setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
                SearchView searchView = (SearchView) item.getActionView();
                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    List<Topic> allTopics = new ArrayList<Topic>(topics);

                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        List<Topic> searchTopics = new ArrayList<Topic>();
                        for (Topic topic : allTopics) {
                            if (topic.getTitle().toLowerCase().contains(newText.toLowerCase()) ||
                                    (topic.getCategory() != null && topic.getCategory().getName().toLowerCase().contains(newText.toLowerCase())) ||
                                    topic.getText().contains(newText.toLowerCase())) {
                                searchTopics.add(topic);
                            }
                        }
                        TopicsAdapter topicsAdapter = (TopicsAdapter) topicNames.getAdapter();
                        topicsAdapter.topics = searchTopics;
                        topicsAdapter.notifyDataSetChanged();
                        return true;
                    }
                });
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
