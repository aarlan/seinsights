package com.premasters.seinsights;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;

public class ActivityLogin extends AppCompatActivity {

    SharedPreferences sPref;
    EditText input_email, input_password;
    Button btn_login;
    final String email = "email";
    final String password = "password";
    final String role = "role";
    final String name = "name";
    NetworkController networkController;
    InternetAccessChecker IAC = new InternetAccessChecker();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        input_email = (EditText) findViewById(R.id.input_email);
        input_password = (EditText) findViewById(R.id.input_password);

        btn_login = (Button) findViewById(R.id.btn_login);

        networkController = new NetworkController(getApplicationContext());

        try {
            loadData();
        } catch (UnsupportedEncodingException e) {
            Logger.getAnonymousLogger().info(e.getMessage());
        }
    }


    public void onClickSkip(View view) {
        Intent i = new Intent(ActivityLogin.this, ActivityTopicList.class);
        startActivity(i);
        super.finish();
    }

    public void onClickSignUp(View view) {
        Intent i = new Intent(ActivityLogin.this, ActivitySignUp.class);
        startActivity(i);
        super.finish();
    }

    public void onClickLogin(View view) throws IOException {
        saveData();

        final Activity a = ActivityLogin.this;
        Handler h = new Handler() {
            @Override
            public void handleMessage(Message msg) {

                if (msg.what != 1) { // code if not connected
                    Toast.makeText(a, "Internet is not available at the moment. Please, try again later.", Toast.LENGTH_SHORT).show();
                } else { // code if connected
                    String response = networkController.authenticate(input_email.getText().toString().toLowerCase(), input_password.getText().toString());
                    if (response.startsWith("-1. 403.")) {
                        Toast.makeText(a, response.substring(9), Toast.LENGTH_SHORT).show();
                    } else if (response.startsWith("0.") && response.charAt(3) == 'A') { // if Access was granted
                        String json = response.substring(19);
                        ObjectMapper mapper = new ObjectMapper();
                        UserIdNameAndRoleId userIdNameAndRoleId = null;
                        try {
                            userIdNameAndRoleId = mapper.readValue(json, UserIdNameAndRoleId.class);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        try {
                            assign_role(userIdNameAndRoleId.getRoleid());
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        try {
                            sPref = getSharedPreferences("userData", MODE_PRIVATE);
                            SharedPreferences.Editor ed = sPref.edit();
                            byte[] bytesName = userIdNameAndRoleId.getName().getBytes("UTF-8");
                            String hashedName = Base64.encodeToString(bytesName, Base64.DEFAULT);
                            ed.putString(name, hashedName);
                            ed.commit();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(a, "Hello, " + userIdNameAndRoleId.getName() + "!", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(ActivityLogin.this, ActivityTopicList.class);
                        startActivity(i);
                        a.finish();
                    } else if (response.startsWith("0.") && response.charAt(3) == 'E') { // if the account is not yet activated (the activation code was not entered)
                        Toast.makeText(a, response.substring(3), Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(ActivityLogin.this, ActivityAuthentication.class);
                        startActivity(i);
                        a.finish();
                    } else if (response.startsWith("{")) {
                        Toast.makeText(a, "Internal server error. Please, try again later.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };

        IAC.isNetworkAvailable(h, 2000, getApplicationContext());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    void assign_role(int roleid) throws UnsupportedEncodingException {
        sPref = getSharedPreferences("userData", MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        byte[] bytesRole = String.valueOf(roleid).getBytes("UTF-8");
        String hashedRole = Base64.encodeToString(bytesRole, Base64.DEFAULT);
        ed.putString(role, hashedRole);
        ed.commit();
    }

    void saveData() throws UnsupportedEncodingException {
        sPref = getSharedPreferences("userData", MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        byte[] bytesEmail = input_email.getText().toString().getBytes("UTF-8");
        byte[] bytesPassword = input_password.getText().toString().getBytes("UTF-8");
        String hashedEmail = Base64.encodeToString(bytesEmail, Base64.DEFAULT);
        String hashedPassword = Base64.encodeToString(bytesPassword, Base64.DEFAULT);
        ed.putString(email, hashedEmail);
        ed.putString(password, hashedPassword);
        ed.commit();
    }

    void loadData() throws UnsupportedEncodingException {
        sPref = getSharedPreferences("userData", MODE_PRIVATE);
        String hashedEmail = sPref.getString(email, "");
        byte[] bytesEmail = Base64.decode(hashedEmail, Base64.DEFAULT);
        String savedEmail = new String(bytesEmail, "UTF-8");
        input_email.setText(savedEmail);
    }
}
