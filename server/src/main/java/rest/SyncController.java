package rest;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by alnedorezov on 3/6/16.
 */

@RestController
public class SyncController {
    Application a = new Application();

    public List<IdClass> getIdsFromTopic(List<Topic> list) throws ParseException {
        List<IdClass> ids = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            ids.add(new IdClass(list.get(i).getId()));
        }
        return ids;
    }

    public List<IdClass> getIdsFromCategory(List<Category> list) throws ParseException {
        List<IdClass> ids = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            ids.add(new IdClass(list.get(i).getId()));
        }
        return ids;
    }

    @RequestMapping("/resources/topics/sync")
    public Sync sync(@RequestParam(value = "date", defaultValue = "2015-07-26 15:00:00.0") String date) throws Exception {
        JdbcConnectionSource connectionSource = new JdbcConnectionSource(a.DATABASE_URL, "h2", "");
        a.setupDatabase(connectionSource, false);
        QueryBuilder<Topic, Integer> qbTopics = a.topicDao.queryBuilder();
        Date modifiedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(date);
        qbTopics.where().ge("created", modifiedDate).or().ge("modified", modifiedDate);
        PreparedQuery<Topic> pcTopics = qbTopics.prepare();
        List<Topic> topics = a.topicDao.query(pcTopics);
        List<Category> categories = a.categoryDao.queryForAll();
        return new Sync(getIdsFromTopic(topics), getIdsFromCategory(categories));
    }
}
