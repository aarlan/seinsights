package rest;

import java.util.List;

/**
 * Created by alnedorezov on 3/3/16.
 */
public class TopicsObject {
    private List<Topic> topics;

    public TopicsObject(List<Topic> topics) {
        this.topics = topics;
    }

    public void addTopic(Topic topic) {
        this.topics.add(topic);
    }

    public void setTopic(int index, Topic topic) {
        this.topics.set(index, topic);
    }

    public void getTopic(int index) {
        this.topics.get(index);
    }

    public void removeTopic(int index) {
        this.topics.remove(index);
    }

    public List<Topic> getTopics() {
        return topics;
    }
}