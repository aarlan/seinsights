package rest;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedDelete;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by alnedorezov on 3/6/16.
 */

@RestController
public class UserLikesTopicsController {
    Application a = new Application();

    @RequestMapping("/resources/topics/userlikestopics")
    public UserLikesTopicsObject userLikesTopics(@RequestParam(value = "userid", defaultValue = "-1") int userid) throws Exception {
        JdbcConnectionSource connectionSource = new JdbcConnectionSource(a.DATABASE_URL, "h2", "");
        a.setupDatabase(connectionSource, false);
        UserLikesTopicsObject ult1;
        if (userid == -1)
            ult1 = new UserLikesTopicsObject(a.userLikesTopicDao.queryForAll());
        else {
            QueryBuilder<UserLikesTopic, Integer> qb = a.userLikesTopicDao.queryBuilder();
            qb.where().eq("user_id", userid);
            PreparedQuery<UserLikesTopic> pc = qb.prepare();
            ult1 = new UserLikesTopicsObject(a.userLikesTopicDao.query(pc));
        }
        connectionSource.close();
        return ult1;
    }

    @RequestMapping(value = "/resources/topics/userlikestopic", method = RequestMethod.POST)
    public String logs(@RequestParam(value = "topicid", defaultValue = "-1") int topicid, @RequestParam(value = "userid", defaultValue = "-1") int userid,
                       @RequestParam(value = "created", defaultValue = "-1") String createdStr) throws Exception {
        JdbcConnectionSource connectionSource = new JdbcConnectionSource(a.DATABASE_URL, "h2", "");
        a.setupDatabase(connectionSource, false);
        if (topicid == -1 || userid == -1) {
            connectionSource.close();
            return "-1. Wrong parameters.";
        } else {
            // Here may be implemented checks if there is such user & topic
            if (!a.topicDao.idExists(topicid)) {
                connectionSource.close();
                return "-1. There is no such topic.\n";
            } else if (createdStr.equals("-1")) {
                System.out.println("Received POST request: user with id=" + userid + " disliked topic with id=" + topicid);
                DeleteBuilder<UserLikesTopic, Integer> db = a.userLikesTopicDao.deleteBuilder();
                db.where().eq("user_id", userid).and().eq("topic_id", topicid);
                PreparedDelete<UserLikesTopic> preparedDelete = db.prepare();
                a.userLikesTopicDao.delete(preparedDelete);
                connectionSource.close();
                return "0. User with id=" + userid + " successfully disliked topic with id=" + topicid + "\n";
            } else {
                System.out.println("Received POST request: user with id=" + userid + " liked topic with id=" + topicid + " on " + createdStr);
                a.userLikesTopicDao.create(new UserLikesTopic(topicid, userid, createdStr));
                connectionSource.close();
                return "0. User with id=" + userid + " successfully liked topic with id=" + topicid + " on " + createdStr + "\n";
            }
        }
    }
}
