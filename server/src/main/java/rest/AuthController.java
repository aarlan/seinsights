package rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by alnedorezov on 4/13/16.
 */

@RestController
public class AuthController {
    Application a = new Application();

    @Autowired
    PasswordEncoder passwordEncoder;

    @RequestMapping(value = "/resources/topics/auth", method = RequestMethod.POST)
    public String logs(@RequestParam(value = "email", defaultValue = "") String email, @RequestParam(value = "password", defaultValue = "") String password) throws Exception {
        JdbcConnectionSource connectionSource = new JdbcConnectionSource(a.DATABASE_URL, "h2", "");
        a.setupDatabase(connectionSource, false);

        QueryBuilder<User, Integer> qb = a.userDao.queryBuilder();
        qb.where().eq("email", email.toLowerCase());
        PreparedQuery<User> pc = qb.prepare();
        List<User> users = a.userDao.query(pc);
        if (users.size() == 0 || (users.size() == 1 && !passwordEncoder.matches(password, users.get(0).getPassword()))) {
            connectionSource.close();
            return "-1. 403. Bad credentials.\n";
        } else if (users.get(0).getActivated() != 100) {
            connectionSource.close();
            return "0. Enter activation code.\n";
        } else {
            QueryBuilder<UserRole, Integer> qb2 = a.userRoleDao.queryBuilder();
            qb2.where().eq("USER_ID", users.get(0).getId());
            PreparedQuery<UserRole> pc2 = qb2.prepare();
            int roleid = a.userRoleDao.query(pc2).get(0).getRole_id();
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(new UserIdNameAndRoleId(users.get(0).getId(), users.get(0).getName(), roleid));
            connectionSource.close();
            return "0. Access granted. " + json + "\n";
        }
    }

    private class UserIdNameAndRoleId {
        private int id;
        private String name;
        private int roleid;

        public UserIdNameAndRoleId(int id, String name, int roleid) {
            this.id = id;
            this.name = name;
            this.roleid = roleid;
        }

        public int getId() {
            return id;
        }

        public int getRoleid() {
            return roleid;
        }

        public String getName() {
            return name;
        }
    }
}