package rest;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by alnedorezov on 3/6/16.
 */
@DatabaseTable(tableName = "UserLikesTopic")
public class UserLikesTopic {
    @DatabaseField(uniqueCombo = true)
    private int topic_id;
    @DatabaseField(uniqueCombo = true)
    private int user_id;
    @DatabaseField
    private Date created = null;

    public UserLikesTopic(int topic_id, int user_id, String createdStr) throws ParseException {
        this.topic_id = topic_id;
        this.user_id = user_id;
        this.created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(createdStr);
    }

    public UserLikesTopic() {
        // all persisted classes must define a no-arg constructor with at least package visibility
    }

    public int getTopic_id() {
        return topic_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public String getCreated() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(created);
    }
}
