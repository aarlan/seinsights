package rest;

/**
 * Created by alnedorezov on 3/3/16.
 */
public class CategoryObject {
    private final Category category;

    public CategoryObject(Category category) {
        this.category = category;
    }

    public Category getCategory() {
        return category;
    }
}