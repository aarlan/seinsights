package rest;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.QueryBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by alnedorezov on 3/3/16.
 */

@RestController
public class TopicsController {
    Application a = new Application();

    @RequestMapping("/resources/topics/topics/all")
    public TopicsObject topics() throws Exception {
        JdbcConnectionSource connectionSource = new JdbcConnectionSource(a.DATABASE_URL, "h2", "");
        a.setupDatabase(connectionSource, false);
        TopicsObject topics1 = new TopicsObject(a.topicDao.queryForAll());
        connectionSource.close();
        return topics1;
    }

    @RequestMapping("/resources/topics/topics")
    public TopicObject topic(@RequestParam(value = "id", defaultValue = "-1") int id) throws Exception {
        JdbcConnectionSource connectionSource = new JdbcConnectionSource(a.DATABASE_URL, "h2", "");
        a.setupDatabase(connectionSource, false);
        Topic topic1 = a.topicDao.queryForId(id);
        connectionSource.close();
        return new TopicObject(topic1);
    }

    @RequestMapping(value = "/resources/topics/topics", method = RequestMethod.POST)
    public String logs(@RequestParam(value = "id", defaultValue = "-1") int id, @RequestParam(value = "title", defaultValue = "") String title,
                       @RequestParam(value = "text", defaultValue = "") String text, @RequestParam(value = "likes", defaultValue = "0") int likes,
                       @RequestParam(value = "created", defaultValue = "-1") String createdStr, @RequestParam(value = "modified", defaultValue = "-2") String modifiedStr,
                       @RequestParam(value = "deleted", defaultValue = "null") String strDeleted, @RequestParam(value = "ready", defaultValue = "null") String strReady) throws Exception {
        boolean ready = true;
        if (strReady.equals("false"))
            ready = false;
        boolean deleted = false;
        if (strDeleted.equals("true"))
            deleted = true;

        JdbcConnectionSource connectionSource = new JdbcConnectionSource(a.DATABASE_URL, "h2", "");
        a.setupDatabase(connectionSource, false);

        if (deleted) {
            // Deleting a topic
            System.out.println("Received POST request: delete topic with id=" + id);
            if (id == -1) {
                connectionSource.close();
                return "-1. Wrong parameters.\n";
            } else if (!a.topicDao.idExists(id)) {
                connectionSource.close();
                return "-1. There is no such topic.\n";
            } else {
                Topic topic1 = a.topicDao.queryForId(id);
                a.topicDao.update(new Topic(id, topic1.getText(), topic1.getTitle(), topic1.getLikes(), topic1.getCreated(), topic1.getModified(), true, topic1.getReady()));
                //For permanent deletion use: a.topicDao.deleteById(id);
                connectionSource.close();
                return "0. Topic with id=" + id + " was successfully deleted.\n";
            }
        } else {
            if (!createdStr.equals("-1") && createdStr.equals(modifiedStr)) {
                // Creating a topic
                System.out.println("Received POST request: create topic with id=" + id);
                a.topicDao.create(new Topic(id, text, title, likes, createdStr, modifiedStr, deleted, ready));
                QueryBuilder<Topic, Integer> qBuilder = a.topicDao.queryBuilder();
                qBuilder.orderBy("id", false); // false for descending order
                qBuilder.limit(1);
                Topic createdTopic = a.topicDao.queryForId(qBuilder.query().get(0).getId());
                System.out.println(createdTopic.getId() + " | " + createdTopic.getText() + " | " +
                        createdTopic.getTitle() + " | " + createdTopic.getLikes() + " | " +
                        createdTopic.getCreated() + " | " + createdTopic.getModified() + " | " +
                        createdTopic.getDeleted() + " | " + createdTopic.getReady());
                connectionSource.close();
                return "0. Topic with id=" + createdTopic.getId() + " was successfully created.\n";
            } else {
                // Updating a topic
                if (!a.topicDao.idExists(id)) {
                    connectionSource.close();
                    return "-1. There is no such topic.\n";
                } else {
                    System.out.println("Received POST request: update topic with id=" + id);
                    TopicUpdate updTopic = checkDataForUpdates(new TopicUpdate(text, title, likes), a.topicDao.queryForId(id));
                    DeletedReady updDeletedReady = checkStrDeletedStrReadyValues(strDeleted, strReady, a.topicDao.queryForId(id));
                    a.topicDao.update(new Topic(id, updTopic.getText(), updTopic.getTitle(), updTopic.getLikes(), createdStr, modifiedStr, updDeletedReady.isDeleted(), updDeletedReady.isReady()));
                    connectionSource.close();
                    return "0. Topic with id=" + id + " was successfully updated.\n";
                }
            }
        }
    }

    private TopicUpdate checkDataForUpdates(TopicUpdate checkedTopicData, Topic topicInDatabase) {
        if (checkedTopicData.getText().equals(""))
            checkedTopicData.setText(topicInDatabase.getText());

        if (checkedTopicData.getTitle().equals(""))
            checkedTopicData.setTitle(topicInDatabase.getTitle());

        if (checkedTopicData.getLikes() <= 0)
            checkedTopicData.setLikes(topicInDatabase.getLikes());

        return checkedTopicData;
    }

    private DeletedReady checkStrDeletedStrReadyValues(String strDeleted, String strReady, Topic topicInDatabase) {
        DeletedReady updatedDeletedReady = new DeletedReady(false, true);

        if (strDeleted.equals("null"))
            updatedDeletedReady.setDeleted(topicInDatabase.getDeleted());
        else if (strDeleted.equals("true"))
            updatedDeletedReady.setDeleted(true);

        if (strReady.equals("null"))
            updatedDeletedReady.setReady(topicInDatabase.getReady());
        else if (strReady.equals("false"))
            updatedDeletedReady.setReady(false);

        return updatedDeletedReady;
    }

    private class TopicUpdate {
        private String text;
        private String title;
        private int likes;

        public TopicUpdate(String text, String title, int likes) {
            this.text = text;
            this.title = title;
            this.likes = likes;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getLikes() {
            return likes;
        }

        public void setLikes(int likes) {
            this.likes = likes;
        }
    }

    private class DeletedReady {
        private boolean deleted;
        private boolean ready;

        public DeletedReady(boolean deleted, boolean ready) {
            this.deleted = deleted;
            this.ready = ready;
        }

        public boolean isDeleted() {
            return deleted;
        }

        public void setDeleted(boolean deleted) {
            this.deleted = deleted;
        }

        public boolean isReady() {
            return ready;
        }

        public void setReady(boolean ready) {
            this.ready = ready;
        }
    }
}
