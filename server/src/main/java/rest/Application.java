package rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * Created by alnedorezov on 2/24/16.
 */

@Configuration
@ComponentScan
@EnableAutoConfiguration
@SpringBootApplication
public class Application {

    public final static String DATABASE_URL = "jdbc:h2:tcp://188.130.155.234:9092/test;IFEXISTS=TRUE";
    // change 'localhost' to 188.130.155.234 for debug and vice versa for deploy

    public Dao<User, Integer> userDao;
    public Dao<Role, Integer> roleDao;
    public Dao<UserRole, Integer> userRoleDao;
    public Dao<Topic, Integer> topicDao;
    public Dao<Category, Integer> categoryDao;
    public Dao<TopicCategory, Integer> topicCategoryDao;
    public Dao<UserLikesTopic, Integer> userLikesTopicDao;

    @Autowired
    private MyBean myBean;

    public static void main(String[] args) throws Exception {

        // Initial connect to the database // for demo values
        new Application().connectToDB();

        // Run Spring application
        SpringApplication.run(Application.class, args);
    }


    private void connectToDB() throws Exception {
        JdbcConnectionSource connectionSource = null;
        try {
            // create our data source
            connectionSource = new JdbcConnectionSource(DATABASE_URL, "h2", "");
            // setup our database and DAOs
            setupDatabase(connectionSource, true);
        } finally {
            // destroy the data source which should close underlying connections
            if (connectionSource != null) {
                //connectionSource.close();
            }
        }
    }

    /**
     * Setup our database and DAOs
     */
    public void setupDatabase(ConnectionSource connectionSource, boolean createTables) throws Exception {

        userDao = DaoManager.createDao(connectionSource, User.class);
        roleDao = DaoManager.createDao(connectionSource, Role.class);
        userRoleDao = DaoManager.createDao(connectionSource, UserRole.class);
        topicDao = DaoManager.createDao(connectionSource, Topic.class);
        categoryDao = DaoManager.createDao(connectionSource, Category.class);
        topicCategoryDao = DaoManager.createDao(connectionSource, TopicCategory.class);
        userLikesTopicDao = DaoManager.createDao(connectionSource, UserLikesTopic.class);

        // if you need to create tables
        if (createTables) {
            TableUtils.createTableIfNotExists(connectionSource, User.class);
            TableUtils.createTableIfNotExists(connectionSource, Role.class);
            TableUtils.createTableIfNotExists(connectionSource, UserRole.class);
            TableUtils.createTableIfNotExists(connectionSource, Topic.class);
            TableUtils.createTableIfNotExists(connectionSource, Category.class);
            TableUtils.createTableIfNotExists(connectionSource, TopicCategory.class);
            TableUtils.createTableIfNotExists(connectionSource, UserLikesTopic.class);
        }
    }
}
