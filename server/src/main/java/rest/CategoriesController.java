package rest;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedDelete;
import com.j256.ormlite.stmt.QueryBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by alnedorezov on 3/6/16.
 */

@RestController
public class CategoriesController {
    Application a = new Application();

    @RequestMapping("/resources/topics/categories")
    public CategoriesObject categories() throws Exception {
        JdbcConnectionSource connectionSource = new JdbcConnectionSource(a.DATABASE_URL, "h2", "");
        a.setupDatabase(connectionSource, false);
        CategoriesObject categories1 = new CategoriesObject(a.categoryDao.queryForAll());
        connectionSource.close();
        return categories1;
    }

    @RequestMapping("/resources/topics/category")
    public CategoryObject categories(@RequestParam(value = "id", defaultValue = "-1") int id) throws Exception {
        JdbcConnectionSource connectionSource = new JdbcConnectionSource(a.DATABASE_URL, "h2", "");
        a.setupDatabase(connectionSource, false);
        Category category1 = a.categoryDao.queryForId(id);
        connectionSource.close();
        return new CategoryObject(category1);
    }

    @RequestMapping(value = "/resources/topics/category", method = RequestMethod.POST)
    public String logs(@RequestParam(value = "id", defaultValue = "-1") int id,
                       @RequestParam(value = "name", defaultValue = "!~DELETE") String name) throws Exception {
        JdbcConnectionSource connectionSource = new JdbcConnectionSource(a.DATABASE_URL, "h2", "");
        a.setupDatabase(connectionSource, false);

        if (name.equals("!~DELETE")) {
            // Deleting a category
            System.out.println("Received POST request: delete category with id=" + id);
            if (id == -1)
                return "-1. Wrong parameters.\n";
            else if (!a.categoryDao.idExists(id)) {
                connectionSource.close();
                return "-1. There is no such category.\n";
            } else {
                a.categoryDao.deleteById(id);
                DeleteBuilder<TopicCategory, Integer> db = a.topicCategoryDao.deleteBuilder();
                db.where().eq("category_id", id);
                PreparedDelete<TopicCategory> preparedDelete = db.prepare();
                a.topicCategoryDao.delete(preparedDelete);
                connectionSource.close();
                return "0. Category with id=" + id + " was successfully deleted.\n";
            }
        } else {
            if (id == -1) {
                // Creating a category
                System.out.println("Received POST request: create category with name=" + name);
                a.categoryDao.create(new Category(id, name));
                QueryBuilder<Category, Integer> qBuilder = a.categoryDao.queryBuilder();
                qBuilder.orderBy("id", false); // false for descending order
                qBuilder.limit(1);
                Category createdCategory = a.categoryDao.queryForId(qBuilder.query().get(0).getId());
                System.out.println(createdCategory.getId() + " | " + createdCategory.getName());
                connectionSource.close();
                return "0. Category with id=" + createdCategory.getId() + " was successfully created.\n";
            } else {
                // Updating a category
                System.out.println("Received POST request: update category with id=" + id);
                if (!a.categoryDao.idExists(id)) {
                    connectionSource.close();
                    return "-1. There is no such category.\n";
                } else {
                    Category category1 = a.categoryDao.queryForId(id);
                    String updName;
                    if (!name.equals("!~DELETE"))
                        updName = name;
                    else
                        updName = category1.getName();
                    a.categoryDao.update(new Category(id, updName));
                    connectionSource.close();
                    return "0. Category with id=" + id + " was successfully updated.\n";
                }
            }
        }
    }
}
