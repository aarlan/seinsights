package rest;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by alnedorezov on 4/14/16.
 */

@RestController
public class ActivationCodeController {
    Application a = new Application();
    Mail m = new Mail();

    @Autowired
    PasswordEncoder passwordEncoder;

    @RequestMapping(value = "/resources/topics/activationcode", method = RequestMethod.POST)
    public String logs(@RequestParam(value = "email", defaultValue = "") String email, @RequestParam(value = "password", defaultValue = "") String password,
                       @RequestParam(value = "activationcode", defaultValue = "") String activationcode) throws Exception {
        JdbcConnectionSource connectionSource = new JdbcConnectionSource(a.DATABASE_URL, "h2", "");
        a.setupDatabase(connectionSource, false);

        QueryBuilder<User, Integer> qb = a.userDao.queryBuilder();
        qb.where().eq("email", email.toLowerCase());
        PreparedQuery<User> pc = qb.prepare();
        List<User> users = a.userDao.query(pc);
        final int maxTries = 4; // max number of tries until the activation code will be changed
        int newActivated;
        int newActivationCode;
        if (users.isEmpty() || (users.size() == 1 && !passwordEncoder.matches(password, users.get(0).getPassword()))) {
            connectionSource.close();
            return "-1. 403. Bad credentials.\n";
        } else if (users.get(0).getActivated() == 100) {
            connectionSource.close();
            return "0. Account is already activated.\n";
        } else if (users.get(0).getActivated() <= maxTries && users.get(0).getActivated() >= 0 && users.get(0).getActivation_code().equals(activationcode)) {
            newActivated = 100;
            a.userDao.update(new User(users.get(0).getId(), users.get(0).getEmail(), users.get(0).getName(),
                    users.get(0).getPassword(), newActivated, users.get(0).getActivation_code(),
                    users.get(0).getCreated(), users.get(0).getDeleted()));
            connectionSource.close();
            m.send(users.get(0).getEmail(), "Software Engineering Insights Team", "Hello, " +
                    users.get(0).getName() + "!\nYour account was successfully activated.");
            return "0. Account is successfully activated.\n";
        } else if (users.get(0).getActivated() == maxTries && !users.get(0).getActivation_code().equals(activationcode)) {
            newActivated = 0;
            newActivationCode = ThreadLocalRandom.current().nextInt(1000, 999999 + 1);
            a.userDao.update(new User(users.get(0).getId(), users.get(0).getEmail(), users.get(0).getName(),
                    users.get(0).getPassword(), newActivated, Integer.toString(newActivationCode),
                    users.get(0).getCreated(), users.get(0).getDeleted()));
            m.send(users.get(0).getEmail(), "Software Engineering Insights Team", "Hello, " +
                    users.get(0).getName() + "!\nPlease enter the following application code in the application: " + newActivationCode);
            connectionSource.close();
            return "-1. Wrong activation code. New activation code was sent to your email.\n";
        } else if (users.get(0).getActivated() < maxTries && users.get(0).getActivated() >= 0 && !users.get(0).getActivation_code().equals(activationcode)) {
            newActivated = users.get(0).getActivated() + 1;
            a.userDao.update(new User(users.get(0).getId(), users.get(0).getEmail(), users.get(0).getName(),
                    users.get(0).getPassword(), newActivated, users.get(0).getActivation_code(),
                    users.get(0).getCreated(), users.get(0).getDeleted()));
            connectionSource.close();
            return "-1. Wrong activation code.\n";
        } else {
            newActivated = 0;
            newActivationCode = ThreadLocalRandom.current().nextInt(1000, 999999 + 1);
            a.userDao.update(new User(users.get(0).getId(), users.get(0).getEmail(), users.get(0).getName(),
                    users.get(0).getPassword(), newActivated, Integer.toString(newActivationCode),
                    users.get(0).getCreated(), users.get(0).getDeleted()));
            m.send(users.get(0).getEmail(), "Software Engineering Insights Team", "Hello, " +
                    users.get(0).getName() + "!\nPlease enter the following application code in the application: " + newActivationCode);
            connectionSource.close();
            return "-1. Internal server error. New activation code was sent to your email.\n";
        }
    }
}
