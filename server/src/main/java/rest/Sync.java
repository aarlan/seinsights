package rest;

import java.util.List;

/**
 * Created by alnedorezov on 3/6/16.
 */
public class Sync {
    private List<IdClass> topicIds;
    private List<IdClass> categoryIds;

    public Sync(List<IdClass> topicIds, List<IdClass> categoryIds) {
        this.topicIds = topicIds;
        this.categoryIds = categoryIds;
    }

    public void addTopicId(IdClass topicId) {
        this.topicIds.add(topicId);
    }

    public void setTopicId(int index, IdClass topicId) {
        this.topicIds.set(index, topicId);
    }

    public void getTopicId(int index) {
        this.topicIds.get(index);
    }

    public void removeTopicId(int index) {
        this.topicIds.remove(index);
    }

    public List<IdClass> getTopics() {
        return topicIds;
    }

    public void addCategoryId(IdClass categoryId) {
        this.categoryIds.add(categoryId);
    }

    public void setCategoryId(int index, IdClass categoryId) {
        this.categoryIds.set(index, categoryId);
    }

    public void getCategoryId(int index) {
        this.categoryIds.get(index);
    }

    public void removeCategoryId(int index) {
        this.categoryIds.remove(index);
    }

    public List<IdClass> getCategories() {
        return categoryIds;
    }
}
