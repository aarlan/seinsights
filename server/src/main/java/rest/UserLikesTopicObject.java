package rest;

/**
 * Created by alnedorezov on 3/6/16.
 */
public class UserLikesTopicObject {
    private final UserLikesTopic userLikesTopic;

    public UserLikesTopicObject(UserLikesTopic userLikesTopic) {
        this.userLikesTopic = userLikesTopic;
    }

    public UserLikesTopic getUserlikestopic() {
        return userLikesTopic;
    }
}
