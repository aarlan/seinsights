package rest;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by alnedorezov on 3/6/16.
 */
@DatabaseTable(tableName = "UserRole")
public class UserRole {
    @DatabaseField(uniqueCombo = true)
    private int role_id;
    @DatabaseField(uniqueCombo = true)
    private int user_id;

    public UserRole(int role_id, int user_id) {
        this.role_id = role_id;
        this.user_id = user_id;
    }

    public UserRole() {
        // all persisted classes must define a no-arg constructor with at least package visibility
    }

    public int getRole_id() {
        return role_id;
    }

    public int getUser_id() {
        return user_id;
    }
}
