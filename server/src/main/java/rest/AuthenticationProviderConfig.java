package rest;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

/**
 * Created by alnedorezov on 4/7/16.
 */

@Configuration
public class AuthenticationProviderConfig {
    Application a = new Application();

    @Bean(name = "dataSource")
    public DriverManagerDataSource dataSource() {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setUrl(a.DATABASE_URL);
        driverManagerDataSource.setUsername("h2");
        driverManagerDataSource.setPassword("");
        return driverManagerDataSource;
    }

    @Bean(name = "userDetailsService")
    public UserDetailsService userDetailsService() {
        JdbcDaoImpl jdbcDaoImpl = new JdbcDaoImpl();
        jdbcDaoImpl.setDataSource(dataSource());
        jdbcDaoImpl.setUsersByUsernameQuery("select email as username, password, activated from user where email=?");
        jdbcDaoImpl.setAuthoritiesByUsernameQuery("select user.email as username, role.name as role from user inner join userrole on user.id=userrole.user_id inner join role on userrole.role_id = role.id where user.email=?");
        return jdbcDaoImpl;
    }
}
