package rest;

/**
 * Created by alnedorezov on 3/6/16.
 */
public class RoleObject {
    private final Role role;

    public RoleObject(Role role) {
        this.role = role;
    }

    public Role getRole() {
        return role;
    }
}
