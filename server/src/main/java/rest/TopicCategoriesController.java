package rest;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedDelete;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by alnedorezov on 3/3/16.
 */

@RestController
public class TopicCategoriesController {
    Application a = new Application();

    @RequestMapping("/resources/topics/topiccategories")
    public TopicCategoriesObject topicCategories() throws Exception {
        JdbcConnectionSource connectionSource = new JdbcConnectionSource(a.DATABASE_URL, "h2", "");
        a.setupDatabase(connectionSource, false);
        TopicCategoriesObject tco1 = new TopicCategoriesObject(a.topicCategoryDao.queryForAll());
        connectionSource.close();
        return tco1;
    }

    @RequestMapping(value = "/resources/topics/topiccategory", method = RequestMethod.POST)
    public String logs(@RequestParam(value = "topicid", defaultValue = "-1") int topicid, @RequestParam(value = "categoryid", defaultValue = "-1") int categoryid) throws Exception {
        JdbcConnectionSource connectionSource = new JdbcConnectionSource(a.DATABASE_URL, "h2", "");
        a.setupDatabase(connectionSource, false);
        QueryBuilder<TopicCategory, Integer> qb = a.topicCategoryDao.queryBuilder();
        qb.where().eq("category_id", categoryid).and().eq("topic_id", topicid);
        PreparedQuery<TopicCategory> pc = qb.prepare();
        List<TopicCategory> topicCategories = a.topicCategoryDao.query(pc);
        String action;
        if (topicCategories.size() > 0)
            action = "delete";
        else
            action = "add";

        if (topicid == -1 || categoryid == -1) {
            connectionSource.close();
            return "-1. Wrong parameters.";
        } else {
            // Here may be implemented checks if there is such category & topic
            if (!a.topicDao.idExists(topicid)) {
                connectionSource.close();
                return "-1. There is no such topic.\n";
            } else if (!a.categoryDao.idExists(categoryid)) {
                connectionSource.close();
                return "-1. There is no such category.\n";
            } else if (action.equals("add")) {
                System.out.println("Received POST request: category with id=" + categoryid + " was assigned to topic with id=" + topicid);
                a.topicCategoryDao.create(new TopicCategory(topicid, categoryid));
                connectionSource.close();
                return "0. Category with id=" + categoryid + " was assigned to topic with id=" + topicid + "\n";
            } else { // if action = delete
                System.out.println("Received POST request: category with id=" + categoryid + " was unassigned from topic with id=" + topicid);
                DeleteBuilder<TopicCategory, Integer> db = a.topicCategoryDao.deleteBuilder();
                db.where().eq("category_id", categoryid).and().eq("topic_id", topicid);
                PreparedDelete<TopicCategory> preparedDelete = db.prepare();
                a.topicCategoryDao.delete(preparedDelete);
                connectionSource.close();
                return "0. Category with id=" + categoryid + " was unassigned from topic with id=" + topicid + "\n";
            }
        }
    }
}
