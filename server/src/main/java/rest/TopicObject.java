package rest;

/**
 * Created by alnedorezov on 2/25/16.
 */
public class TopicObject {
    private final Topic topic;

    public TopicObject(Topic topic) {
        this.topic = topic;
    }

    public Topic getTopic() {
        return topic;
    }
}