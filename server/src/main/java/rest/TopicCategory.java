package rest;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by alnedorezov on 3/3/16.
 */
@DatabaseTable(tableName = "TopicCategory")
public class TopicCategory {
    @DatabaseField(uniqueCombo = true)
    private int topic_id;
    @DatabaseField(uniqueCombo = true)
    private int category_id;

    public TopicCategory(int topic_id, int category_id) {
        this.topic_id = topic_id;
        this.category_id = category_id;
    }

    public TopicCategory() {
        // all persisted classes must define a no-arg constructor with at least package visibility
    }

    public int getTopic_id() {
        return topic_id;
    }

    public int getCategory_id() {
        return category_id;
    }
}
