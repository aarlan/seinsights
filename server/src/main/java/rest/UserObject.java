package rest;

/**
 * Created by alnedorezov on 3/6/16.
 */
public class UserObject {
    private final User user;

    public UserObject(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
