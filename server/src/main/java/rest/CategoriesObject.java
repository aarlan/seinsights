package rest;

import java.util.List;

/**
 * Created by alnedorezov on 3/6/16.
 */
public class CategoriesObject {
    private List<Category> categories;

    public CategoriesObject(List<Category> categories) {
        this.categories = categories;
    }

    public void addCategory(Category category) {
        this.categories.add(category);
    }

    public void setCategory(int index, Category category) {
        this.categories.set(index, category);
    }

    public void getCategory(int index) {
        this.categories.get(index);
    }

    public void removeCategory(int index) {
        this.categories.remove(index);
    }

    public List<Category> getCategories() {
        return categories;
    }
}
