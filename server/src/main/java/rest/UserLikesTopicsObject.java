package rest;

import java.util.List;

/**
 * Created by alnedorezov on 3/6/16.
 */
public class UserLikesTopicsObject {
    private List<UserLikesTopic> likedTopics;

    public UserLikesTopicsObject(List<UserLikesTopic> likedTopics) {
        this.likedTopics = likedTopics;
    }

    public void addLikedTopic(UserLikesTopic likedTopic) {
        this.likedTopics.add(likedTopic);
    }

    public void setLikedTopic(int index, UserLikesTopic likedTopic) {
        this.likedTopics.set(index, likedTopic);
    }

    public void getLikedTopic(int index) {
        this.likedTopics.get(index);
    }

    public void removeLikedTopic(int index) {
        this.likedTopics.remove(index);
    }

    public List<UserLikesTopic> getLikedtopics() {
        return likedTopics;
    }
}
