/*
 * Copyright 2010-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package rest;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

/**
 * Created by alnedorezov on 4/7/16.
 */

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, "/resources/topics/topics/all").hasAnyRole("Administrator", "Reader", "Guest")
                .antMatchers(HttpMethod.POST, "/resources/topics/userlikestopic").hasRole("Reader")
                .antMatchers(HttpMethod.GET, "/resources/topics/users").hasAnyRole("Administrator", "Reader", "Guest")
                .antMatchers(HttpMethod.POST, "/resources/topics/users").hasAnyRole("Administrator", "Reader", "Guest")
                .antMatchers(HttpMethod.GET, "/resources/topics/userlikestopics").hasAnyRole("Administrator", "Reader", "Guest")
                .antMatchers(HttpMethod.GET, "/resources/topics/roles").hasAnyRole("Administrator", "Reader", "Guest")
                .antMatchers(HttpMethod.GET, "/resources/topics/role").hasAnyRole("Administrator", "Reader", "Guest")
                .antMatchers(HttpMethod.POST, "/resources/topics/role").hasRole("Administrator")
                .antMatchers(HttpMethod.GET, "/resources/topics/userroles").hasAnyRole("Administrator", "Reader", "Guest")
                .antMatchers(HttpMethod.GET, "/resources/topics/userrole").hasAnyRole("Administrator", "Reader", "Guest")
                .antMatchers(HttpMethod.POST, "/resources/topics/userrole").hasRole("Administrator")
                .antMatchers(HttpMethod.GET, "/resources/topics/topics").hasAnyRole("Administrator", "Reader", "Guest")
                .antMatchers(HttpMethod.POST, "/resources/topics/topics").hasRole("Administrator")
                .antMatchers(HttpMethod.GET, "/resources/topics/topiccategory").hasAnyRole("Administrator", "Reader", "Guest")
                .antMatchers(HttpMethod.POST, "/resources/topics/topiccategory").hasRole("Administrator")
                .antMatchers(HttpMethod.GET, "/resources/topics/category").hasAnyRole("Administrator", "Reader", "Guest")
                .antMatchers(HttpMethod.POST, "/resources/topics/category").hasRole("Administrator")
                .antMatchers(HttpMethod.GET, "/resources/topics/topiccategories").hasAnyRole("Administrator", "Reader", "Guest")
                .antMatchers(HttpMethod.GET, "/resources/topics/categories").hasAnyRole("Administrator", "Reader", "Guest")
                .antMatchers(HttpMethod.POST, "/resources/topics/auth").hasRole("Guest")
                .antMatchers(HttpMethod.POST, "/resources/topics/activationcode").hasRole("Guest")
                .antMatchers(HttpMethod.POST, "/resources/topics/register").hasRole("Guest")
                .antMatchers(HttpMethod.GET, "/resources/topics/sync").hasAnyRole("Administrator", "Reader", "Guest")
                .anyRequest().permitAll()
                .and()
                .exceptionHandling()
                .and()
                .httpBasic();

        http.authorizeRequests()
                .antMatchers("/resources/topics/**").access("hasRole('Reader') or hasRole('Administrator') or hasRole('Guest')")
                .anyRequest().denyAll()
                .and()
                .exceptionHandling()
                .and()
                .httpBasic();

        http.csrf().disable();
    }

    @Bean(name = "passwordEncoder")
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}