package rest;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by alnedorezov on 2/24/16.
 */
@DatabaseTable(tableName = "Topic")
public class Topic {
    @DatabaseField(generatedId = true, unique = true)
    private int id;
    @DatabaseField
    private String text;
    @DatabaseField
    private String title;
    @DatabaseField
    private int likes;
    @DatabaseField
    private Date created = null;
    @DatabaseField
    private Date modified = null;
    @DatabaseField
    private boolean deleted;
    @DatabaseField
    private boolean ready;

    public Topic(int id, String text, String title, int likes, String createdStr, String modifiedStr, boolean deleted, boolean ready) throws ParseException {
        this.id = id;
        this.text = text;
        this.title = title;
        this.likes = likes;
        this.created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(createdStr);
        this.modified = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").parse(modifiedStr);
        this.deleted = deleted;
        this.ready = ready;
    }

    public Topic() {
        // all persisted classes must define a no-arg constructor with at least package visibility
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        return title;
    }

    public int getLikes() {
        return likes;
    }

    public String getCreated() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(created);
    }

    public String getModified() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(modified);
    }

    public boolean getDeleted() {
        return deleted;
    }

    public boolean getReady() {
        return ready;
    }
}
