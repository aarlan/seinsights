package rest;

/**
 * Created by alnedorezov on 4/12/16.
 */
public class UserRoleObject {
    private final UserRole userrole;

    public UserRoleObject(UserRole userrole) {
        this.userrole = userrole;
    }

    public UserRole getUserrole() {
        return userrole;
    }
}
