package rest;

import java.util.List;

/**
 * Created by alnedorezov on 3/3/16.
 */
public class TopicCategoriesObject {
    private List<TopicCategory> topicCategories;

    public TopicCategoriesObject(List<TopicCategory> topicCategories) {
        this.topicCategories = topicCategories;
    }

    public void addTopicCategory(TopicCategory topicCategory) {
        this.topicCategories.add(topicCategory);
    }

    public void setTopicCategory(int index, TopicCategory topicCategory) {
        this.topicCategories.set(index, topicCategory);
    }

    public void getTopicCategory(int index) {
        this.topicCategories.get(index);
    }

    public void removeTopicCategory(int index) {
        this.topicCategories.remove(index);
    }

    public List<TopicCategory> getTopiccategories() {
        return topicCategories;
    }
}
