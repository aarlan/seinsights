package rest;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by alnedorezov on 4/18/16.
 */

@RestController
public class RegistrationController {
    Application a = new Application();
    Mail m = new Mail();

    @Autowired
    PasswordEncoder passwordEncoder;

    @RequestMapping(value = "/resources/topics/register", method = RequestMethod.POST)
    public String logs(@RequestParam(value = "email", defaultValue = "") String email, @RequestParam(value = "password", defaultValue = "") String password,
                       @RequestParam(value = "name", defaultValue = "") String name) throws Exception {
        JdbcConnectionSource connectionSource = new JdbcConnectionSource(a.DATABASE_URL, "h2", "");
        a.setupDatabase(connectionSource, false);

        int newActivated;
        int newActivationCode;
        String errorMessage = "";

        QueryBuilder<User, Integer> qb = a.userDao.queryBuilder();
        qb.where().eq("email", email);
        PreparedQuery<User> pc = qb.prepare();
        List<User> users = a.userDao.query(pc);
        if (!users.isEmpty())
            errorMessage += "Account with email " + email + " already exists. ";
        if (email.isEmpty())
            errorMessage += "Email should be set. ";
        if (password.length() < 4)
            errorMessage += "Password should contain at least 4 characters. ";
        if (name.isEmpty())
            errorMessage += "Name should be set. ";

        if (errorMessage.isEmpty()) {
            newActivated = 0;
            newActivationCode = ThreadLocalRandom.current().nextInt(1000, 999999 + 1);
            Date created = new Date();
            String createdStr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(created);
            a.userDao.create(new User(0, email.toLowerCase(), name, passwordEncoder.encode(password), newActivated, Integer.toString(newActivationCode), createdStr, false));
            QueryBuilder<User, Integer> qBuilder = a.userDao.queryBuilder();
            qBuilder.orderBy("id", false); // false for descending order
            qBuilder.limit(1);
            User createdUser = a.userDao.queryForId(qBuilder.query().get(0).getId());
            a.userRoleDao.create(new UserRole(2, createdUser.getId()));
            m.send(email, "Software Engineering Insights Team", "Hello, " +
                    name + "!\nPlease enter the following application code in the application: " + newActivationCode);
            connectionSource.close();
            return "0. Account was successfully created. The activation code was sent to your email.\n";
        } else {
            connectionSource.close();
            return "-1. " + errorMessage + "\n";
        }
    }
}
