# README #

### Contents ###

* mobile - sources for the Android application. It can be run with Android Studio.
* server - Java Gradle project. It can be run with IntelliJ Idea.

### What is this repository for? ###

* This project is about development of the mobile application which informs users about best practices in Software Engineering, motivates users to learn best practices in Software Engineering and advertizes MSIT-SE program at Innopolis University.